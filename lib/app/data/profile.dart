class Profile {
  String? id;
  String? username;
  String? imageUrl;

  Profile({this.id, this.username, this.imageUrl});

  factory Profile.fromJson(Map<String, dynamic> json) {
    return Profile(
      id: json['id'],
      username: json['username'] ?? 'Placeholder',
      imageUrl: json['imageUrl'] ??
          'https://i.playboard.app/p/AAUvwnhAYg2vYjxEY6fcu0RGcAJKOXZ3AXbXS98jjdpR/default.jpg',
    );
  }

  set changeUname(String newUsername) {
    username = newUsername;
  }

  set changeImgUrl(String newUrl) {
    imageUrl = newUrl;
  }
}
