class Member {
  String? name;
  String? content;
  String? imageUrl;

  Member({this.name, this.content, this.imageUrl});

  factory Member.fromJson(Map<String, dynamic> json) {
    return Member(
      name: json['name'],
      content: json['content'],
      imageUrl: json['imageUrl'],
    );
  }
}
