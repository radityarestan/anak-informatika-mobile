import 'package:anak_informatika/app/data/models.dart';

class Post {
  String? id;
  Profile? author;
  String? title;
  String? content;
  DateTime? postTime;
  int? likeCount;
  int? commentCount;
  bool? isLiked;
  bool? isBookmarked;
  List<Comment>? comments;

  Post({
    this.id,
    this.author,
    this.title,
    this.content,
    this.postTime,
    this.likeCount,
    this.commentCount,
    this.isLiked,
    this.isBookmarked,
    this.comments,
  });

  factory Post.fromJson(Map<String, dynamic> json) {
    if (json['comments'] == null) {
      return Post(
        id: json['id'],
        author: Profile.fromJson(json['author'] ?? {}),
        title: json['title'],
        content: json['content'],
        postTime: DateTime.fromMillisecondsSinceEpoch(
            json['postTime']['_seconds'] * 1000),
        likeCount: json['likeCount'] ?? 0,
        commentCount: json['commentCount'] ?? 0,
        isLiked: json['isLiked'] ?? false,
        isBookmarked: json['isBookmarked'] ?? false,
      );
    }

    return Post(
      id: json['id'],
      author: Profile.fromJson(json['author'] ?? {}),
      title: json['title'],
      content: json['content'],
      postTime: DateTime.fromMillisecondsSinceEpoch(
          json['postTime']['_seconds'] * 1000),
      likeCount: json['likeCount'] ?? 0,
      commentCount: json['commentCount'] ?? 0,
      isLiked: json['isLiked'] ?? false,
      isBookmarked: json['isBookmarked'] ?? false,
      comments: (json['comments'] as List)
          .map((comment) => Comment.fromJson(comment))
          .toList(),
    );
  }
}
