import 'package:anak_informatika/app/data/models.dart';

class Comment {
  Profile? author;
  String? content;
  DateTime? postTime;

  Comment({this.author, this.content, this.postTime});

  factory Comment.fromJson(Map<String, dynamic> json) {
    return Comment(
      author: Profile.fromJson(json['author'] ?? {}),
      content: json['comment'],
      postTime: DateTime.fromMillisecondsSinceEpoch(
          json['commentTime']['_seconds'] * 1000),
    );
  }
}
