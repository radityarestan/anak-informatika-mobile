import 'dart:convert';
import 'dart:io';
import 'dart:math';

import 'package:anak_informatika/app/data/profile.dart';
import 'package:anak_informatika/app/services/services.dart';
import 'package:anak_informatika/app/shared/keys.dart';
import 'package:anak_informatika/app/shared/widgets/loading.dart';

import 'package:crypto/crypto.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/services.dart';
import 'package:get_storage/get_storage.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:get/get.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';

class Firebase {
  final _signInkey = Keys.isAlreadySignedIn;

  final _box = GetStorage();
  final _googleSignIn = GoogleSignIn();
  final _firebase = FirebaseAuth.instance;
  final _apiProvider = Get.find<ApiProvider>();
  final _firebaseStorage = FirebaseStorage.instance.ref();

  late GoogleSignInAccount? _googleAccount;
  late AuthorizationCredentialAppleID? _appleAccount;
  late String? _rawNonce;

  Future<String> signUp(String email, String password) async {
    var errorMessage = Error.doesNotExist;

    try {
      Get.dialog(const Loading());
      await _firebase.createUserWithEmailAndPassword(
          email: email, password: password);
      await _box.write(_signInkey, true);
      await _createProfileFromAccount(email);
    } on FirebaseAuthException catch (error) {
      errorMessage = _getFirebaseException(error);
    } on PlatformException catch (_) {
      errorMessage = 'error_unexpected'.tr;
    } catch (e) {
      errorMessage = 'error_unexpected'.tr;
    } finally {
      Get.back();
    }
    return errorMessage;
  }

  Future<String> signIn(String email, String password) async {
    var errorMessage = Error.doesNotExist;

    try {
      Get.dialog(const Loading());
      await _firebase.signInWithEmailAndPassword(
          email: email, password: password);
      await _box.write(_signInkey, true);
    } on FirebaseAuthException catch (error) {
      errorMessage = _getFirebaseException(error);
    } on PlatformException catch (_) {
      errorMessage = 'error_unexpected'.tr;
    } catch (e) {
      errorMessage = 'error_unexpected'.tr;
    } finally {
      Get.back();
    }

    return errorMessage;
  }

  Future<String> signOut() async {
    var errorMessage = Error.doesNotExist;

    try {
      Get.dialog(const Loading());
      await _firebase.signOut();
      await _googleSignIn.signOut();
      await _box.remove(_signInkey);
      await _box.write(_signInkey, false);
    } on FirebaseAuthException catch (error) {
      errorMessage = _getFirebaseException(error);
    } on PlatformException catch (_) {
      errorMessage = 'error_unexpected'.tr;
    } catch (e) {
      errorMessage = 'error_unexpected'.tr;
    } finally {
      Get.back();
    }

    return errorMessage;
  }

  Future<bool> getGoogleAccount() async {
    _googleAccount = await _googleSignIn.signIn();
    if (_googleAccount == null) {
      return false;
    } else {
      return true;
    }
  }

  Future<String> authWithGoogle(bool isSignUp) async {
    var errorMessage = Error.doesNotExist;

    try {
      Get.dialog(const Loading());
      var googleAuth = await _googleAccount!.authentication;
      var credential = GoogleAuthProvider.credential(
        idToken: googleAuth.idToken,
        accessToken: googleAuth.accessToken,
      );

      var authResult = await _firebase.signInWithCredential(credential);
      await _box.write(_signInkey, true);
      if (isSignUp && authResult.user != null) {
        var email = authResult.user!.email;
        var imageUrl = authResult.user!.photoURL;
        await _createProfileFromAccount(email!, imageUrl: imageUrl!);
      }
    } on FirebaseAuthException catch (error) {
      errorMessage = _getFirebaseException(error);
      _googleSignIn.signOut();
    } on PlatformException catch (_) {
      errorMessage = 'error_unexpected'.tr;
      _googleSignIn.signOut();
    } catch (e) {
      errorMessage = 'error_unexpected'.tr;
      _googleSignIn.signOut();
    } finally {
      Get.back();
    }

    return errorMessage;
  }

  Future<String> changePassword(email) async {
    var errorMessage = Error.doesNotExist;

    try {
      Get.dialog(const Loading());
      await _firebase.sendPasswordResetEmail(email: email);
    } on FirebaseAuthException catch (error) {
      errorMessage = _getFirebaseException(error);
      _googleSignIn.signOut();
    } on PlatformException catch (_) {
      errorMessage = 'error_unexpected'.tr;
      _googleSignIn.signOut();
    } catch (e) {
      errorMessage = 'error_unexpected'.tr;
      _googleSignIn.signOut();
    } finally {
      Get.back();
    }

    return errorMessage;
  }

  String _generateRawNonce({int length = 32}) {
    const charset =
        '1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    final random = Random.secure();

    String output =
        List.generate(length, (_) => charset[random.nextInt(charset.length)])
            .join();

    return output;
  }

  String _generateNonceSha256(String rawNonce) {
    return sha256.convert(utf8.encode(rawNonce)).toString();
  }

  Future<bool> getAppleAccount() async {
    try {
      _rawNonce = _generateRawNonce();
      var nonce = _generateNonceSha256(_rawNonce!);

      _appleAccount = await SignInWithApple.getAppleIDCredential(
        scopes: [
          AppleIDAuthorizationScopes.email,
          AppleIDAuthorizationScopes.fullName,
        ],
        nonce: nonce,
      );
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<String> authWithApple(bool isSignUp) async {
    var errorMessage = Error.doesNotExist;

    try {
      var credential = OAuthProvider('apple.com').credential(
        idToken: _appleAccount?.identityToken,
        rawNonce: _rawNonce,
      );
      var authResult = await _firebase.signInWithCredential(credential);
      await _box.write(_signInkey, true);
      if (isSignUp && authResult.user != null) {
        var email = authResult.user!.email;
        var imageUrl = authResult.user!.photoURL;
        await _createProfileFromAccount(email!, imageUrl: imageUrl!);
      }
    } catch (e) {
      errorMessage = 'error_unexpected'.tr;
    } finally {
      Get.back();
    }

    return errorMessage;
  }

  Future<void> _createProfileFromAccount(String email,
      {String imageUrl =
          'https://firebasestorage.googleapis.com/v0/b/anak-informatika-f2699.appspot.com/o/uploads%2Fdata%2Fuser%2Fprofile.png?alt=media&token=4a5b3922-607c-4201-a2ba-f31136589f3d'}) async {
    String username = email.substring(0, email.indexOf('@'));
    Profile newProfile = Profile(username: username, imageUrl: imageUrl);
    await _apiProvider.createProfile(newProfile);
  }

  Future<String> uploadImageToFirebase(File imageFile) async {
    var uploadTask = await _firebaseStorage
        .child('uploads/${imageFile.path}')
        .putFile(imageFile);
    return uploadTask.ref.getDownloadURL().then((value) => value);
  }

  String _getFirebaseException(FirebaseAuthException error) {
    var _errorMessage = '';
    switch (error.code) {
      case Error.invalidEmail:
        _errorMessage = 'error_invalidEmail';
        break;
      case Error.emailAlreadyInUse:
        _errorMessage = 'error_emailAlreadyInUse'.tr;
        break;
      case Error.weakPassword:
        _errorMessage = 'error_weakPassword'.tr;
        break;
      case Error.wrongPassword:
        _errorMessage = 'error_wrongPassword'.tr;
        break;
      case Error.userNotFound:
        _errorMessage = 'error_userNotFound'.tr;
        break;
      case Error.userDisabled:
        _errorMessage = 'error_userDisabled'.tr;
        break;
      case Error.tooManyRequest:
        _errorMessage = 'error_tooManyRequest'.tr;
        break;
      case Error.operationNotAllowed:
        _errorMessage = 'error_operationNotAllowed'.tr;
        break;
      case Error.differentCredential:
        _errorMessage = 'error_differentCredential'.tr;
        break;
      case Error.invalidCredential:
        _errorMessage = 'error_invalidCredential'.tr;
        break;
      default:
        _errorMessage = 'error_unexpected'.tr;
    }

    return _errorMessage;
  }
}
