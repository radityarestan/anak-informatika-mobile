import 'dart:convert';

import 'package:anak_informatika/app/data/member.dart';
import 'package:anak_informatika/app/data/models.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';
import 'package:get/get_connect/http/src/request/request.dart';

class ApiProvider extends GetConnect {
  late String _token;

  static const String host =
      'https://us-central1-anak-informatika-f2699.cloudfunctions.net';
  static const String postPath = '/post';
  static const String likePath = postPath + '/like';
  static const String unlikePath = postPath + '/unlike';
  static const String commentPath = postPath + '/comment';
  static const String bookmarkPath = '/bookmark';
  static const String profilePath = '/profile';
  static const String aboutPath = '/about';

  @override
  void onInit() {
    httpClient.baseUrl = host;
    httpClient.defaultContentType = 'application/json';

    httpClient.addRequestModifier((Request request) {
      request.headers['Authorization'] = 'Bearer $_token';
      return request;
    });

    super.onInit();
  }

  Future<void> createPost(Post inputPost) async {
    _token = await FirebaseAuth.instance.currentUser!.getIdToken(true);

    var body = json.encode({
      'title': inputPost.title,
      'content': inputPost.content,
    });

    await post(postPath, body);
  }

  Future<Post> getPostById(String postId) async {
    _token = await FirebaseAuth.instance.currentUser!.getIdToken(true);

    final response = await get(postPath + '/' + postId);
    return Post.fromJson(response.body);
  }

  Future<List<Post>> getRecommendedPost() async {
    _token = await FirebaseAuth.instance.currentUser!.getIdToken(true);

    if (httpClient.baseUrl == null) return [];
    var response = await get(postPath);
    var responseList = response.body as List;
    return responseList.map((postJson) => Post.fromJson(postJson)).toList();
  }

  Future<void> updatePost(Post inputPost) async {
    _token = await FirebaseAuth.instance.currentUser!.getIdToken(true);

    if (inputPost.id == null) return;

    var body = json.encode({
      'title': inputPost.title,
      'content': inputPost.content,
    });
    await put(postPath + '/' + inputPost.id!, body);
  }

  Future<void> deletePost(Post inputPost) async {
    _token = await FirebaseAuth.instance.currentUser!.getIdToken(true);

    if (inputPost.id == null) return;
    await delete(postPath + '/' + inputPost.id!);
  }

  Future<Profile> getProfile() async {
    _token = await FirebaseAuth.instance.currentUser!.getIdToken(true);

    var response = await get(profilePath);
    var responseJson = response.body;
    return Profile.fromJson(responseJson);
  }

  Future<void> createProfile(Profile profile) async {
    _token = await FirebaseAuth.instance.currentUser!.getIdToken(true);

    var body = json.encode({
      'username': profile.username,
      'imageUrl': profile.imageUrl,
    });

    await post(profilePath, body);
  }

  Future<void> updateProfile(Profile profile) async {
    _token = await FirebaseAuth.instance.currentUser!.getIdToken(true);

    var body = json.encode({
      'username': profile.username,
      'imageUrl': profile.imageUrl,
    });

    await put(profilePath, body);
  }

  Future<void> likePost(Post inputPost) async {
    if (inputPost.id == null) return;
    return await likePostById(inputPost.id!);
  }

  Future<void> likePostById(String postId) async {
    _token = await FirebaseAuth.instance.currentUser!.getIdToken(true);
    await post(likePath + '/' + postId, json.encode({}));
  }

  Future<void> unlikePost(Post inputPost) async {
    if (inputPost.id == null) return;
    return await unlikePostById(inputPost.id!);
  }

  Future<void> unlikePostById(String postId) async {
    _token = await FirebaseAuth.instance.currentUser!.getIdToken(true);
    await delete(unlikePath + '/' + postId);
  }

  Future<void> bookmarkPost(Post inputPost) async {
    if (inputPost.id == null) return;
    return await bookmarkPostById(inputPost.id!);
  }

  Future<void> bookmarkPostById(String postId) async {
    _token = await FirebaseAuth.instance.currentUser!.getIdToken(true);
    await post(bookmarkPath + '/' + postId, json.encode({}));
  }

  Future<void> unbookmarkPost(Post inputPost) async {
    if (inputPost.id == null) return;
    return await unbookmarkPostById(inputPost.id!);
  }

  Future<void> unbookmarkPostById(String postId) async {
    _token = await FirebaseAuth.instance.currentUser!.getIdToken(true);
    await delete(bookmarkPath + '/' + postId);
  }

  Future<List<Member>> getMembers() async {
    _token = await FirebaseAuth.instance.currentUser!.getIdToken(true);

    var response = await get(aboutPath);
    var responseList = response.body as List;

    return responseList.map((aboutJson) => Member.fromJson(aboutJson)).toList();
  }

  Future<void> createComment(String postId, String comment) async {
    _token = await FirebaseAuth.instance.currentUser!.getIdToken(true);

    var body = json.encode({'comment': comment});
    await post(commentPath + '/' + postId, body);
  }
}
