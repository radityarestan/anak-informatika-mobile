import 'package:anak_informatika/app/modules/bar/bar_controller.dart';
import 'package:anak_informatika/app/modules/bookmark/bookmark_view.dart';
import 'package:anak_informatika/app/modules/home/home_view.dart';
import 'package:anak_informatika/app/modules/settings/settings.dart';
import 'package:anak_informatika/app/shared/theme/constant.dart';
import 'package:anak_informatika/app/shared/theme/theme_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AuthenticatedBar extends GetView<BarController> {
  const AuthenticatedBar({Key? key}) : super(key: key);

  buildBottomNavigationMenu(context) {
    return Obx(
      () => BottomNavigationBar(
        currentIndex: controller.tabIndex.value,
        onTap: controller.selectPage,
        type: BottomNavigationBarType.fixed,
        selectedItemColor: kThemeController.primaryMain,
        unselectedItemColor: kThemeController.neutralOutline,
        selectedLabelStyle: kTheme.textTheme.bodyText2,
        backgroundColor: kThemeController.neutralSurface2,
        items: [
          BottomNavigationBarItem(
            icon: const Icon(Icons.home),
            label: 'navbar_home'.tr,
          ),
          BottomNavigationBarItem(
            icon: const Icon(Icons.bookmark),
            label: 'navbar_bookmark'.tr,
          ),
          BottomNavigationBarItem(
            icon: const Icon(Icons.settings),
            label: 'navbar_settings'.tr,
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ThemeController>(
      builder: (themeController) => Scaffold(
        appBar: AppBar(
          backgroundColor: themeController.appBarColor,
          title: Text(
            'appName'.tr,
            style: kTheme.textTheme.subtitle1!.copyWith(
              color: themeController.primaryOnMain,
            ),
          ),
        ),
        body: Obx(
          () => IndexedStack(
            index: controller.tabIndex.value,
            children: const [
              Home(),
              Bookmark(),
              SettingsView(),
            ],
          ),
        ),
        bottomNavigationBar: buildBottomNavigationMenu(context),
      ),
    );
  }
}
