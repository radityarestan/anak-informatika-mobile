import 'package:anak_informatika/app/modules/bar/bar_controller.dart';
import 'package:anak_informatika/app/modules/settings/settings_controller.dart';
import 'package:anak_informatika/app/modules/post/post_controller.dart';
import 'package:get/get.dart';

class BarBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(BarController());
    Get.put(PostController());
    Get.lazyPut(() => SettingsController());
  }
}
