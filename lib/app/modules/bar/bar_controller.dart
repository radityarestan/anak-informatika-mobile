import 'package:anak_informatika/app/modules/bookmark/bookmark_controller.dart';
import 'package:anak_informatika/app/modules/home/home.dart';
import 'package:get/get.dart';

class BarController extends GetxController {
  final _bookmarkController = Get.put(BookmarkController());
  final _homeController = Get.put(HomeController());

  final _page = {'home': 0, 'bookmark': 1};

  var tabIndex = 0.obs;

  void selectPage(int index) {
    tabIndex.value = index;
    if (index == _page['home']) {
      _homeController.update();
    } else if (index == _page['bookmark']) {
      _bookmarkController.update();
    }
  }
}
