import 'package:anak_informatika/app/shared/keys.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class TranslationController extends GetxController {
  final _box = GetStorage();
  final _key = Keys.languageCode;

  @override
  void onInit() async {
    super.onInit();

    var langCode = _box.read(_key) ?? 'en';
    var locale = Locale(langCode);
    await Get.updateLocale(locale);
  }

  void changeLanguage(String languageCode) async {
    var locale = Locale(languageCode);
    await _box.write(_key, languageCode);
    await Get.updateLocale(locale);
  }
}
