import 'package:anak_informatika/app/modules/settings/translations/translations.dart';
import 'package:anak_informatika/app/shared/devices/size.dart';
import 'package:anak_informatika/app/shared/theme/theme.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TranslationView extends GetView<TranslationController> {
  const TranslationView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size().init(context, true, false);

    return GetBuilder<ThemeController>(
      builder: (themeController) => Scaffold(
        backgroundColor: themeController.neutralSurface,
        appBar: AppBar(
          backgroundColor: themeController.appBarColor,
          title: Text(
            'changeLanguage_appBar'.tr,
          ),
        ),
        body: Center(
          child: Column(
            children: [
              Container(
                decoration: BoxDecoration(
                  border: Border(
                    bottom:
                        BorderSide(color: kThemeController.neutralOnBackground),
                  ),
                ),
                child: SizedBox(
                  width: Size.w(100),
                  child: Material(
                    color: Colors.transparent,
                    child: InkWell(
                        onTap: () {
                          controller.changeLanguage('id');
                        },
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(8, 16, 8, 16),
                          child: Text(
                            'Indonesia',
                            style: kTheme.textTheme.bodyText1!.copyWith(
                                color: kThemeController.neutralOnBackground),
                          ),
                        )),
                  ),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(
                            color: themeController.neutralOnBackground))),
                child: SizedBox(
                  width: Size.w(100),
                  child: Material(
                    color: Colors.transparent,
                    child: InkWell(
                      onTap: () {
                        controller.changeLanguage('en');
                      },
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(8, 16, 8, 16),
                        child: Text(
                          'English',
                          style: kTheme.textTheme.bodyText1!.copyWith(
                              color: kThemeController.neutralOnBackground),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
