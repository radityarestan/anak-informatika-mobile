import 'package:get/get.dart';

class Messages extends Translations {
  // use page_name to be a key in map below
  // for similar widget, use widget_name to be a key in map below
  @override
  Map<String, Map<String, String>> get keys => {
        'en': {
          'appName': 'Anak Informatika',

          // page content
          'splash_appName': 'Anak\nInformatika',

          'landing_greeting': 'Hello World',
          'landing_greetingSubtitle': 'Welcome to Anak Informatika!',

          'login_greeting': 'Welcome Back!',
          'login_greetingSubtitle': 'Use an existing account',
          'login_forgotPw': 'Forgot Password?',
          'login_choices': 'or sign in with',
          'login_dontHaveAccount': 'Don\'t have an account?',

          'signUp_greeting': 'Welcome!',
          'signUp_greetingSubtitle': 'Create a new account',
          'signUp_choices': 'or sign up with',
          'signUp_haveAccount': 'Already have an account?',

          'homepage_subheading': 'Recommended for You',
          'createPost_markdownDesc': 'This is how your text looks like: ',

          'commentMaker_replyTo': 'Replying to ',

          'errorPage_somethingWentWrong': 'Something went wrong',
          'errorPage_noData': 'There is no data',

          'settings_darkmode': 'Darkmode',
          'settings_changeProfile': 'Change Profile',
          'settings_changeLanguage': 'Change Language',
          'settings_aboutUs': 'About Us',

          'changeProfile_appBar': 'Profile Settings',
          'changeLanguage_appBar': 'Change Language',
          'aboutUs_appBar': 'About Us',

          // widget content
          'button_signIn': 'Sign In',
          'button_signUp': 'Sign Up',
          'button_signInGoogle': 'Sign in with Google',
          'button_signUpGoogle': 'Sign up with Google',
          'button_signInApple': 'Sign in with Apple',
          'button_signUpApple': 'Sign up with Apple',
          'button_signOut': 'Sign Out',
          'button_save': 'Save',
          'button_upload': 'Upload',
          'button_resetPassword': 'Reset Password',
          'button_createComment': 'Create Comment',
          'button_tryAgain': 'TRY AGAIN',

          'input_email': 'Email address',
          'input_password': 'Password',
          'input_confirmPassword': 'Confirm password',
          'input_username': 'Username',
          'input_title': 'Title',
          'input_titleDesc': 'Write a title',
          'input_content': 'Content',
          'input_contentDesc': 'How programmers open a Jar?',
          'input_comment': 'Write your comment',

          'error_invalidEmail': 'Email address is not valid.',
          'error_emailAlreadyInUse':
              'There already exist an account with this email.',
          'error_wrongPassword': 'Your password is wrong.',
          'error_weakPassword': 'Your password is not strong enough.',
          'error_userNotFound':
              'Your account doesn\'t exist. Please sign up first.',
          'error_userDisabled': 'Your email has been disabled.',
          'error_tooManyRequest': 'Too many requests. Try again later.',
          'error_operationNotAllowed':
              'The developer has disabled authentication with this method.',
          'error_unexpected': 'An unexpected error has occured.',
          'error_differentCredential':
              'There already exist an account with the email address asserted by the credential.',
          'error_invalidCredential':
              'The credential is malformed or has expired.',
          'error_emptyField': 'Please enter some text.',
          'error_emailField': 'Please use a valid email address.',
          'error_passwordField': 'Password must contain at least 6 characters.',
          'error_pwConfirmField': 'Please make sure your password match.',
          'error_usernameField':
              'Please use a valid username with 8-20 characters.',

          'navbar_home': 'Home',
          'navbar_bookmark': 'Bookmark',
          'navbar_settings': 'Settings',

          'pictureSource_camera': 'Open camera',
          'pictureSource_gallery': 'Select photo from gallery',

          'postcard_dateNow': 'just now',
          'postcard_dateYear': 'year ago',
          'postcard_dateYears': 'years ago',
          'postcard_dateMonth': 'month ago',
          'postcard_dateMonths': 'months ago',
          'postcard_dateWeek': 'week ago',
          'postcard_dateWeeks': 'weeks ago',
          'postcard_dateDay': 'day ago',
          'postcard_dateDays': 'days ago',
          'postcard_dateHour': 'hour ago',
          'postcard_dateHours': 'hours ago',
          'postcard_dateMinute': 'minute ago',
          'postcard_dateMinutes': 'minutes ago',

          'snackbar_resetPasswordTitle': 'Reset Password',
          'snackbar_resetPasswordMessage':
              'Please check your email to reset password.',
        },
        'id': {
          'appName': 'Anak Informatika',

          // page content
          'splash_appName': 'Anak\nInformatika',

          'landing_greeting': 'Halo Dunia',
          'landing_greetingSubtitle': 'Selamat Datang di Anak Informatika!',

          'login_greeting': 'Selamat Datang Kembali!',
          'login_greetingSubtitle': 'Gunakan akun yang sudah ada',
          'login_forgotPw': 'Lupa Kata Sandi?',
          'login_choices': 'atau masuk dengan',
          'login_dontHaveAccount': 'Tidak punya akun?',

          'signUp_greeting': 'Selamat datang!',
          'signUp_greetingSubtitle': 'Buat akun baru',
          'signUp_choices': 'atau daftar dengan',
          'signUp_haveAccount': 'Sudah punya akun?',

          'homepage_subheading': 'Direkomendasikan untuk Anda',
          'createPost_markdownDesc':
              'Ini adalah bagaimana teks anda terlihat: ',

          'commentMaker_replyTo': 'Membalas untuk ',

          'errorPage_somethingWentWrong': 'Ada yang salah',
          'errorPage_noData': 'Tidak ada data',

          'settings_darkmode': 'Mode Gelap',
          'settings_changeProfile': 'Ganti Profil',
          'settings_changeLanguage': 'Ganti Bahasa',
          'settings_aboutUs': 'Tentang Kami',

          'changeProfile_appBar': 'Pengaturan Profil',
          'changeLanguage_appBar': 'Ganti Bahasa',
          'aboutUs_appBar': 'Tentang Kami',

          // widget content
          'button_signIn': 'Masuk',
          'button_signUp': 'Daftar',
          'button_signInGoogle': 'Masuk dengan Google',
          'button_signUpGoogle': 'Daftar dengan Google',
          'button_signInApple': 'Masuk dengan Apple',
          'button_signUpApple': 'Daftar dengan Apple',
          'button_signOut': 'Keluar',
          'button_save': 'Simpan',
          'button_upload': 'Unggah',
          'button_resetPassword': 'Atur ulang Kata Sandi',
          'button_createComment': 'Buat Komentar',
          'button_tryAgain': 'COBA KEMBALI',

          'input_email': 'Alamat email',
          'input_password': 'Kata sandi',
          'input_confirmPassword': 'Konfirmasi kata sandi',
          'input_username': 'Username',
          'input_title': 'Judul',
          'input_titleDesc': 'Tulis sebuah judul',
          'input_content': 'Konten',
          'input_contentDesc': 'Bagaimana programmer membuka sebuah Jar?',
          'input_comment': 'Tulis komentar Anda',

          'error_invalidEmail': 'Alamat email tidak valid.',
          'error_emailAlreadyInUse':
              'Terdapat akun dengan email yang Anda masukkan.',
          'error_wrongPassword': 'Password Anda salah.',
          'error_weakPassword': 'Password Anda belum cukup kuat.',
          'error_userNotFound':
              'Akun tidak tersedia. Silahkan buat akun terlebih dahulu.',
          'error_userDisabled': 'Email anda telah dinonaktifkan.',
          'error_tooManyRequest':
              'Terlalu banyak permintaan. Coba kembali nanti.',
          'error_operationNotAllowed':
              'Developer telah menonaktifkan autentikasi menggunakan metode ini.',
          'error_unexpected': 'Error tidak terduga telah terjadi.',
          'error_differentCredential':
              'Terdapat akun dengan email yang Anda masukkan dengan kredensial berbeda.',
          'error_invalidCredential': 'Kredensial Anda telah kadaluarsa.',
          'error_emptyField': 'Tolong masukkan beberapa teks.',
          'error_emailField': 'Tolong gunakan alamat email yang valid.',
          'error_passwordField':
              'Kata sandi harus berisi setidaknya 6 karakter.',
          'error_pwConfirmField':
              'Pastikan password yang diisi sama dengan sebelumnya.',
          'error_usernameField':
              'Tolong masukkan username yang valid dengan 8-20 karakter.',

          'navbar_home': 'Beranda',
          'navbar_bookmark': 'Penanda',
          'navbar_settings': 'Pengaturan',

          'pictureSource_camera': 'Buka kamera',
          'pictureSource_gallery': 'Pilih foto dari galeri',

          'postcard_dateNow': 'baru saja',
          'postcard_dateYear': 'tahun lalu',
          'postcard_dateYears': 'tahun lalu',
          'postcard_dateMonth': 'bulan lalu',
          'postcard_dateMonths': 'bulan lalu',
          'postcard_dateWeek': 'minggu lalu',
          'postcard_dateWeeks': 'minggu lalu',
          'postcard_dateDay': 'hari lalu',
          'postcard_dateDays': 'hari lalu',
          'postcard_dateHour': 'jam lalu',
          'postcard_dateHours': 'jam lalu',
          'postcard_dateMinute': 'menit lalu',
          'postcard_dateMinutes': 'menit lalu',

          'snackbar_resetPasswordTitle': 'Ubah Password',
          'snackbar_resetPasswordMessage':
              'Tolong periksa email Anda untuk mengubah password.',
        },
      };
}
