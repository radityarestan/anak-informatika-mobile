import 'package:anak_informatika/app/data/models.dart';
import 'package:anak_informatika/app/modules/settings/settings.dart';
import 'package:anak_informatika/app/shared/theme/theme.dart';
import 'package:anak_informatika/app/shared/devices/size.dart';
import 'package:anak_informatika/app/shared/widgets/widgets.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SettingsView extends GetView<SettingsController> {
  const SettingsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ThemeController>(
      builder: (_) => Scaffold(
        backgroundColor: kThemeController.neutralBackground,
        body: Padding(
          padding: EdgeInsets.symmetric(
            vertical: Size.h(1),
            horizontal: Size.w(5),
          ),
          child: Column(
            children: [
              _buildProfile(),
              SizedBox(height: Size.h(2)),
              _buildSettingsMenu(),
              SizedBox(height: Size.h(2)),
              SquaredButton(
                title: 'button_signOut'.tr,
                width: Size.w(100),
                onTap: () {
                  controller.signOut();
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  _buildProfile() {
    return GetBuilder<SettingsController>(
      builder: (_) => FutureBuilder<Profile>(
          future: controller.getProfile(),
          builder: (_, snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
                return const Text('None');
              case ConnectionState.waiting:
                return Column(
                  children: [
                    CustomShimmer.circular(
                        width: Size.w(20), height: Size.w(20)),
                    SizedBox(height: Size.h(1)),
                    CustomShimmer.rectangular(
                        width: Size.w(50), height: Size.h(5))
                  ],
                );
              case ConnectionState.active:
                return const Text('Active');
              case ConnectionState.done:
                if (snapshot.hasError) {
                  return const Text('Error');
                } else if (!snapshot.hasData) {
                  return const Text('No data');
                }
                return Column(
                  children: [
                    CircleAvatar(
                      radius: Size.w(10),
                      backgroundImage: NetworkImage(snapshot.data!.imageUrl!),
                    ),
                    SizedBox(height: Size.h(1)),
                    AutoSizeText(
                      snapshot.data!.username!,
                      style: kTheme.textTheme.headline3!.copyWith(
                          color: kThemeController.neutralOnBackground),
                    ),
                  ],
                );
            }
          }),
    );
  }

  _buildSettingsMenu() {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: kThemeController.neutralOnBackground),
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        children: [
          _buildMenuWithSwitchInside(
            title: 'settings_darkmode'.tr,
          ),
          InkWell(
            onTap: () {
              controller.toProfileSettings();
            },
            child: _buildMenu(
              title: 'settings_changeProfile'.tr,
            ),
          ),
          InkWell(
            onTap: () {
              controller.toLanguageSettings();
            },
            child: _buildMenu(
              title: 'settings_changeLanguage'.tr,
            ),
          ),
          InkWell(
            borderRadius: const BorderRadius.only(
                bottomLeft: Radius.circular(10),
                bottomRight: Radius.circular(10)),
            onTap: () {
              controller.toAbout();
            },
            child: _buildMenu(
              isBottom: true,
              title: 'settings_aboutUs'.tr,
            ),
          ),
        ],
      ),
    );
  }

  _buildMenuWithSwitchInside({required String title}) {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(color: kThemeController.neutralOnBackground),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.only(left: 8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              title,
              style: kTheme.textTheme.bodyText1!.copyWith(
                color: kThemeController.neutralOnBackground,
              ),
            ),
            Switch(
              value: controller.isDarkMode,
              onChanged: controller.changeSwitchState,
              activeColor: kThemeController.primaryMain,
            ),
          ],
        ),
      ),
    );
  }

  _buildMenu({required String title, bool isBottom = false}) {
    return Container(
      decoration: BoxDecoration(
        border: isBottom
            ? const Border()
            : Border(
                bottom: BorderSide(color: kThemeController.neutralOnBackground),
              ),
      ),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(8, 12, 8, 12),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              title,
              style: kTheme.textTheme.bodyText1!.copyWith(
                color: kThemeController.neutralOnBackground,
              ),
            ),
            Icon(
              Icons.arrow_forward_ios_outlined,
              color: kThemeController.neutralOnBackground,
            ),
          ],
        ),
      ),
    );
  }
}
