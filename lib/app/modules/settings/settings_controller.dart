import 'package:anak_informatika/app/data/member.dart';
import 'package:anak_informatika/app/data/profile.dart';
import 'package:anak_informatika/app/modules/settings/profile_settings/image_controller.dart';
import 'package:anak_informatika/app/services/services.dart';
import 'package:anak_informatika/app/shared/keys.dart';
import 'package:anak_informatika/app/shared/theme/theme.dart';
import 'package:anak_informatika/app/routes/routes.dart';
import 'package:anak_informatika/app/shared/widgets/loading.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class SettingsController extends GetxController {
  final _usernameRegex =
      RegExp(r'^(?=.{8,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$');

  final TextEditingController usernameController = TextEditingController();
  final GlobalKey<FormState> profileFormKey = GlobalKey<FormState>();

  final _isDarkModeKey = Keys.isDarkMode;
  final _box = GetStorage();

  late ThemeController _themeController;
  late ApiProvider _apiService;
  late Firebase _firebaseService;

  bool isDarkMode = false;
  String? imageUrl;

  @override
  void onInit() async {
    super.onInit();
    _themeController = Get.find<ThemeController>();
    _apiService = Get.find<ApiProvider>();
    _firebaseService = Firebase();
    isDarkMode = _box.read(_isDarkModeKey) ?? false;
  }

  @override
  void onClose() {
    super.onClose();
    usernameController.dispose();
  }

  Future<Profile> getProfile() async {
    Profile myProfile = await _apiService.getProfile();
    usernameController.text = myProfile.username!;
    imageUrl = myProfile.imageUrl!;
    return myProfile;
  }

  Future<List<Member>> getMembers() async {
    return await _apiService.getMembers();
  }

  Future<void> refreshPages() async {
    update();
  }

  void changeSwitchState(bool newValue) {
    isDarkMode = newValue;
    _themeController.saveTheme(isDarkMode);
    update();
  }

  void toLanguageSettings() {
    Get.toNamed(AppRoutes.settings + AppRoutes.translation);
  }

  void toProfileSettings() {
    Get.toNamed(AppRoutes.settings + AppRoutes.profile);
  }

  void toAbout() {
    Get.toNamed(AppRoutes.settings + AppRoutes.about);
  }

  Future<void> signOut() async {
    var _result = await _firebaseService.signOut();
    if (_result == Error.doesNotExist) {
      Get.offAllNamed(AppRoutes.auth);
    } else {
      showError(_result);
    }
  }

  void showError(String result) {
    Get.snackbar(
      'Error',
      result,
      backgroundColor: kThemeController.errorMain,
      colorText: kThemeController.errorOnMain,
      snackPosition: SnackPosition.BOTTOM,
      margin: const EdgeInsets.all(10.0),
    );
  }

  String? validateInputContent(String? content) {
    if (content!.isEmpty) {
      return 'error_emptyField'.tr;
    } else if (!_usernameRegex.hasMatch(content)) {
      return 'Please use a valid username with 8-20 characters.';
    }
    return null;
  }

  Future<void> saveProfile() async {
    var _imgCntrl = Get.find<ImageController>();

    if (profileFormKey.currentState!.validate()) {
      Get.dialog(const Loading());
      var username = usernameController.text;
      var profileImage = _imgCntrl.profileImage;

      if (profileImage is! String) {
        profileImage =
            await _firebaseService.uploadImageToFirebase(profileImage);
      }

      Profile newProfile = Profile(username: username, imageUrl: profileImage);
      await _apiService.updateProfile(newProfile);
      update();
      Get.back();
    }
  }
}
