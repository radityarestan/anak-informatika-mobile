import 'package:anak_informatika/app/data/member.dart';
import 'package:anak_informatika/app/modules/settings/settings.dart';
import 'package:anak_informatika/app/shared/theme/theme.dart';
import 'package:anak_informatika/app/shared/devices/size.dart';
import 'package:anak_informatika/app/shared/widgets/widgets.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class About extends GetView<SettingsController> {
  const About({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ThemeController>(
      builder: (themeController) => Scaffold(
          backgroundColor: themeController.neutralBackground,
          appBar: AppBar(
            backgroundColor: themeController.appBarColor,
            title: Text('aboutUs_appBar'.tr),
          ),
          body: Padding(
            padding: const EdgeInsets.all(8),
            child: FutureBuilder<List<Member>>(
              future: controller.getMembers(),
              builder: (_, snapshot) {
                switch (snapshot.connectionState) {
                  case ConnectionState.none:
                    return const Text('Connection not yet started');
                  case ConnectionState.active:
                    return const Text('Active');
                  case ConnectionState.waiting:
                    return const Loading();
                  case ConnectionState.done:
                    if (snapshot.hasError) {
                      return _buildErrorPage('errorPage_somethingWentWrong'.tr);
                    } else if (!snapshot.hasData) {
                      return _buildErrorPage('errorPage_noData'.tr);
                    }
                    return _buildPosts(snapshot);
                }
              },
            ),
          )),
    );
  }

  _buildErrorPage(String errorDescription) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            width: Size.w(30),
            height: Size.w(30),
            child: Image.asset('assets/images/sorry.png'),
          ),
          SizedBox(height: Size.h(1)),
          Text(
            errorDescription,
            style: kTheme.textTheme.bodyText1!.copyWith(
              color: kThemeController.neutralOnBackground,
            ),
          ),
          SizedBox(height: Size.h(5)),
          SquaredButton(
            title: 'button_tryAgain'.tr,
            onTap: () {
              controller.refreshPages();
            },
          ),
        ],
      ),
    );
  }

  Widget _buildPosts(AsyncSnapshot<List<Member>> snapshot) {
    return ListView.builder(
        itemCount: snapshot.data!.length,
        itemBuilder: (_, index) {
          Member member = snapshot.data![index];

          return Row(
            children: [
              SizedBox(
                width: Size.w(30),
                height: Size.h(15),
                child: Image.network(member.imageUrl!),
              ),
              SizedBox(width: Size.w(5)),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  AutoSizeText(
                    member.name!,
                    style: kTheme.textTheme.bodyText1!.copyWith(
                      color: kThemeController.neutralOnBackground,
                    ),
                  ),
                  AutoSizeText(
                    member.content!,
                    style: kTheme.textTheme.bodyText1!.copyWith(
                      color: kThemeController.neutralOnBackground,
                    ),
                  ),
                ],
              ),
            ],
          );
        });
  }
}
