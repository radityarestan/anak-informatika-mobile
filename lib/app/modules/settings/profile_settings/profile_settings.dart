import 'package:anak_informatika/app/data/profile.dart';
import 'package:anak_informatika/app/modules/settings/profile_settings/image_controller.dart';
import 'package:anak_informatika/app/modules/settings/settings.dart';
import 'package:anak_informatika/app/shared/devices/size.dart';
import 'package:anak_informatika/app/shared/theme/theme.dart';
import 'package:anak_informatika/app/shared/widgets/widgets.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ProfileSettings extends GetView<SettingsController> {
  final imgCntrl = Get.put(ImageController());

  ProfileSettings({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ThemeController>(
      builder: (_) => Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: kThemeController.neutralBackground,
        appBar: AppBar(
          backgroundColor: kThemeController.appBarColor,
          title: Text('changeProfile_appBar'.tr),
        ),
        body: Padding(
          padding: EdgeInsets.symmetric(
            vertical: Size.h(1),
            horizontal: Size.w(5),
          ),
          child: Form(
            key: controller.profileFormKey,
            child: Column(
              children: [
                GestureDetector(
                  onTap: () {
                    _buildDialog(context);
                  },
                  child: _buildProfilePhotoGetter(),
                ),
                SizedBox(height: Size.h(2)),
                Input(
                  width: Size.w(90),
                  controller: controller.usernameController,
                  validator: controller.validateInputContent,
                  prefixIcon: const Icon(Icons.person),
                  inputField: 'input_username'.tr,
                  labelText: 'input_username'.tr,
                ),
                SizedBox(height: Size.h(5)),
                SquaredButton(
                  onTap: () {
                    controller.saveProfile();
                  },
                  title: 'button_save'.tr,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  _buildDialog(BuildContext context) {
    return showDialog(
      context: context,
      builder: (_) => AlertDialog(
        backgroundColor: kThemeController.neutralBackground,
        contentPadding: EdgeInsets.zero,
        content: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildImgSource(
              sourceType: ImageSource.gallery,
              source: 'pictureSource_gallery'.tr,
            ),
            _buildImgSource(
              sourceType: ImageSource.camera,
              source: 'pictureSource_camera'.tr,
            ),
          ],
        ),
      ),
    );
  }

  _buildProfilePhotoGetter() {
    return FutureBuilder<Profile>(
      future: controller.getProfile(),
      builder: (_, snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
            return const Text('None');
          case ConnectionState.waiting:
            return CustomShimmer.circular(
                width: Size.w(20), height: Size.w(20));
          case ConnectionState.active:
            return const Text('Active');
          case ConnectionState.done:
            if (snapshot.hasError) {
              return const Text('Error');
            } else if (!snapshot.hasData) {
              return const Text('No data');
            }

            return GetBuilder<ImageController>(
              builder: (imgCntrl) => Container(
                height: Size.w(20),
                width: Size.w(20),
                decoration: BoxDecoration(
                  image: DecorationImage(
                      image: imgCntrl.profileImage is String
                          ? NetworkImage(imgCntrl.profileImage)
                          : FileImage(imgCntrl.profileImage) as ImageProvider),
                  border:
                      Border.all(color: kThemeController.neutralOnBackground),
                  shape: BoxShape.circle,
                ),
                child: Center(
                  child: Icon(
                    Icons.camera_alt,
                    color: kThemeController.neutralOnBackground,
                  ),
                ),
              ),
            );
        }
      },
    );
  }

  _buildImgSource({required ImageSource sourceType, required String source}) {
    return SizedBox(
      width: Size.w(100),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          onTap: () async {
            imgCntrl.getImage(sourceType);
          },
          child: Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
            child: Text(
              source,
              style: kTheme.textTheme.bodyText1!.copyWith(
                color: kThemeController.neutralOnBackground,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
