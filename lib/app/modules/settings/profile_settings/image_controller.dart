import 'dart:io';

import 'package:anak_informatika/app/modules/settings/settings_controller.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

class ImageController extends GetxController {
  final _picker = ImagePicker();
  final _setCntrl = Get.find<SettingsController>();

  dynamic profileImage;

  @override
  void onInit() {
    super.onInit();
    profileImage = _setCntrl.imageUrl;
  }

  Future<void> getImage(ImageSource sourceType) async {
    var pickedFile = await _picker.pickImage(source: sourceType);

    if (pickedFile != null) {
      profileImage = File(pickedFile.path);
      update();
    }
  }
}
