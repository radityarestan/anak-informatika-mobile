import 'package:anak_informatika/app/shared/theme/theme.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SplashView extends StatelessWidget {
  const SplashView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ThemeController>(
      builder: (themeController) => Scaffold(
        backgroundColor: themeController.neutralBackground,
        body: Center(
          child: AutoSizeText(
            'splash_appName'.tr,
            maxLines: 2,
            textAlign: TextAlign.center,
            style: kTheme.textTheme.headline1!.copyWith(
              fontWeight: FontWeight.w900,
              letterSpacing: 2.0,
            ),
          ),
        ),
      ),
    );
  }
}
