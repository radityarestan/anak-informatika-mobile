import 'package:anak_informatika/app/shared/keys.dart';
import 'package:get/get.dart';
import 'package:anak_informatika/app/routes/routes.dart';
import 'package:get_storage/get_storage.dart';

class SplashController extends GetxController {
  final _box = GetStorage();
  final _key = Keys.isAlreadySignedIn;

  @override
  void onReady() async {
    super.onReady();
    await Future.delayed(const Duration(seconds: 2));

    var _isAlreadySignedIn = await _box.read(_key);

    if (_isAlreadySignedIn == null || !_isAlreadySignedIn) {
      Get.offNamed(AppRoutes.auth);
    } else {
      Get.offNamed(AppRoutes.authenticatedBar);
    }
  }
}
