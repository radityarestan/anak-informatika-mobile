import 'package:anak_informatika/app/data/models.dart';
import 'package:anak_informatika/app/routes/routes.dart';
import 'package:anak_informatika/app/services/api_provider.dart';
import 'package:get/get.dart';

class BookmarkController extends GetxController {
  final _apiService = Get.find<ApiProvider>();

  Future<List<Post>> getPosts() async {
    List<Post> allPost = await _apiService.getRecommendedPost();
    List<Post> bookmarkedPost =
        allPost.where((post) => post.isBookmarked!).toList();
    return bookmarkedPost;
  }

  Future<void> refreshPages() async {
    update();
  }

  Future<void> likePost(String postId) async {
    await _apiService.likePostById(postId);
  }

  Future<void> unlikePost(String postId) async {
    await _apiService.unlikePostById(postId);
  }

  Future<void> bookmarkPost(String postId) async {
    await _apiService.bookmarkPostById(postId);
  }

  Future<void> unbookmarkPost(String postId) async {
    await _apiService.unbookmarkPostById(postId);
  }

  void toViewPost(String postId, String authorname) {
    Get.toNamed(AppRoutes.viewPost, arguments: [postId, authorname]);
  }

  void toCommentMaker(String postId, String authorname) {
    Get.toNamed(AppRoutes.viewPost + AppRoutes.createComent,
        arguments: [postId, authorname]);
  }
}
