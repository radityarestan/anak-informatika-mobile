import 'package:anak_informatika/app/modules/authentication/authentication.dart';
import 'package:anak_informatika/app/shared/devices/size.dart';
import 'package:anak_informatika/app/shared/theme/theme.dart';
import 'package:anak_informatika/app/shared/widgets/widgets.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AuthenticationView extends GetView<AuthenticationController> {
  const AuthenticationView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size().init(context, false, false);

    return GetBuilder<ThemeController>(
      builder: (themeController) => Scaffold(
        backgroundColor: themeController.neutralBackground,
        body: Padding(
          padding: EdgeInsets.symmetric(
            vertical: MediaQuery.of(context).padding.top,
            horizontal: Size.w(5),
          ),
          child: Padding(
            padding: EdgeInsets.only(top: Size.h(10)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                AutoSizeText(
                  'landing_greeting'.tr,
                  maxLines: 1,
                  style: kTheme.textTheme.headline1!.copyWith(
                    color: themeController.neutralOnBackground,
                  ),
                ),
                AutoSizeText(
                  'landing_greetingSubtitle'.tr,
                  maxLines: 1,
                  style: kTheme.textTheme.subtitle1!.copyWith(
                    color: themeController.neutralOutline,
                  ),
                ),
                SizedBox(height: Size.h(5)),
                SquaredButton(
                  width: Size.w(90),
                  title: 'button_signIn'.tr,
                  onTap: () {
                    controller.toSignIn();
                  },
                ),
                SizedBox(height: Size.h(5)),
                SquaredButton(
                  width: Size.w(90),
                  title: 'button_signUp'.tr,
                  onTap: () {
                    controller.toSignUp();
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
