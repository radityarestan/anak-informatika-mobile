import 'package:anak_informatika/app/modules/authentication/authentication_controller.dart';
import 'package:anak_informatika/app/shared/devices/size.dart';
import 'package:anak_informatika/app/shared/theme/theme.dart';
import 'package:anak_informatika/app/shared/widgets/widgets.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RegisterView extends GetView<AuthenticationController> {
  const RegisterView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size().init(context, true, false);

    return GetBuilder<ThemeController>(
      builder: (themeController) => Scaffold(
        backgroundColor: themeController.neutralBackground,
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          leading: BackButton(color: themeController.neutralOnBackground),
          backgroundColor: Colors.transparent,
          foregroundColor: themeController.neutralOutline,
          elevation: 0,
        ),
        body: Padding(
          padding: EdgeInsets.symmetric(horizontal: Size.w(5)),
          child: Form(
            key: controller.registerFormKey,
            child: Column(
              children: [
                SizedBox(height: Size.h(2)),
                Align(
                  alignment: Alignment.centerLeft,
                  child: AutoSizeText(
                    'signUp_greeting'.tr,
                    maxLines: 1,
                    style: kTheme.textTheme.headline1!.copyWith(
                      color: themeController.neutralOnBackground,
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: AutoSizeText(
                    'signUp_greetingSubtitle'.tr,
                    maxLines: 1,
                    style: kTheme.textTheme.subtitle1!.copyWith(
                      color: themeController.neutralOutline,
                    ),
                  ),
                ),
                SizedBox(height: Size.h(2)),
                Input(
                  width: Size.w(90),
                  prefixIcon: const Icon(Icons.email_outlined),
                  inputField: 'input_email'.tr,
                  controller: controller.emailController,
                  validator: (email) {
                    return controller.validateMail(email);
                  },
                ),
                SizedBox(height: Size.h(2)),
                Input(
                  width: Size.w(90),
                  prefixIcon: const Icon(Icons.lock_outlined),
                  inputField: 'input_password'.tr,
                  controller: controller.passwordController,
                  obscureText: true,
                  validator: (password) {
                    return controller.validatePassword(password);
                  },
                ),
                SizedBox(height: Size.h(2)),
                Input(
                  width: Size.w(90),
                  prefixIcon: const Icon(Icons.lock_outlined),
                  inputField: 'input_confirmPassword'.tr,
                  controller: controller.pwConfirmationController,
                  obscureText: true,
                  validator: (password) {
                    return controller.validatePwConfirmation(password);
                  },
                ),
                SizedBox(height: Size.h(5)),
                SquaredButton(
                  title: 'button_signUp'.tr,
                  onTap: () async {
                    await controller.signUpWithEmailAndPassword();
                  },
                  width: Size.w(90),
                ),
                SizedBox(height: Size.h(4)),
                AutoSizeText(
                  'signUp_choices'.tr,
                  maxLines: 1,
                  style: kTheme.textTheme.bodyText1!.copyWith(
                    color: themeController.neutralOutline,
                  ),
                ),
                SizedBox(height: Size.h(4)),
                SocialButton(
                  assetName: 'assets/images/google.png',
                  title: 'button_signUpGoogle'.tr,
                  width: Size.w(90),
                  onTap: () async {
                    await controller.authWithGoogle(isSignUp: true);
                  },
                ),
                if (GetPlatform.isIOS) SizedBox(height: Size.h(1)),
                if (GetPlatform.isIOS)
                  SocialButton(
                    assetName: 'assets/images/apple.svg',
                    title: 'button_signUpApple'.tr,
                    width: Size.w(90),
                    onTap: () async {
                      await controller.authWithApple(isSignUp: true);
                    },
                  ),
                SizedBox(height: Size.h(10)),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    AutoSizeText(
                      'signUp_haveAccount'.tr,
                      maxLines: 1,
                      style: kTheme.textTheme.bodyText1!.copyWith(
                        color: themeController.neutralOutline,
                      ),
                    ),
                    SquaredButtonNoFill(
                      title: 'button_signIn'.tr,
                      onTap: () {
                        controller.toSignIn();
                      },
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
