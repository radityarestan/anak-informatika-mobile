import 'package:anak_informatika/app/routes/routes.dart';
import 'package:anak_informatika/app/services/services.dart';
import 'package:anak_informatika/app/shared/theme/theme.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AuthenticationController extends GetxController {
  final _emailRegex = RegExp(
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');
  late GlobalKey<FormState> registerFormKey;
  late GlobalKey<FormState> loginFormKey;

  late Firebase _firebaseService;
  late TextEditingController emailController;
  late TextEditingController passwordController;
  late TextEditingController pwConfirmationController;

  @override
  void onInit() {
    super.onInit();
    _firebaseService = Firebase();
    emailController = TextEditingController();
    passwordController = TextEditingController();
    pwConfirmationController = TextEditingController();
  }

  @override
  void onClose() {
    super.onClose();
    emailController.dispose();
    passwordController.dispose();
    pwConfirmationController.dispose();
  }

  String? validateMail(String? email) {
    if (email!.isEmpty) {
      return 'error_emptyField'.tr;
    } else if (!_emailRegex.hasMatch(email)) {
      return 'error_emailField'.tr;
    }
    return null;
  }

  String? validatePassword(String? password) {
    if (password!.isEmpty) {
      return 'error_emptyField'.tr;
    } else if (password.length < 6) {
      return 'error_passwordField'.tr;
    }
    return null;
  }

  String? validatePwConfirmation(String? passwordConfirmation) {
    String password = passwordController.text;
    if (passwordConfirmation!.isEmpty) {
      return 'error_emptyField'.tr;
    } else if (password != passwordConfirmation) {
      return 'error_pwConfirmField'.tr;
    }
    return null;
  }

  Future<void> signUpWithEmailAndPassword() async {
    if (registerFormKey.currentState!.validate()) {
      var _email = emailController.text;
      var _password = passwordController.text;
      var _result = await _firebaseService.signUp(_email, _password);
      _checkAuthResult(_result);
    }
  }

  Future<void> signInWithEmailAndPassword() async {
    if (loginFormKey.currentState!.validate()) {
      var _email = emailController.text;
      var _password = passwordController.text;
      var _result = await _firebaseService.signIn(_email, _password);
      _checkAuthResult(_result);
    }
  }

  Future<void> authWithGoogle({required bool isSignUp}) async {
    var _isAccountExist = await _firebaseService.getGoogleAccount();
    if (_isAccountExist) {
      var _result = await _firebaseService.authWithGoogle(isSignUp);
      _checkAuthResult(_result);
    }
  }

  Future<void> authWithApple({required bool isSignUp}) async {
    var _isAccountExist = await _firebaseService.getAppleAccount();
    if (_isAccountExist) {
      var _result = await _firebaseService.authWithApple(isSignUp);
      _checkAuthResult(_result);
    }
  }

  Future<void> changePassword(String email) async {
    var result = await _firebaseService.changePassword(email);

    if (result == Error.doesNotExist) {
      Get.snackbar(
        'snackbar_resetPasswordTitle'.tr,
        'snackbar_resetPasswordMessage'.tr,
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: kThemeController.successMain,
        colorText: kThemeController.successOnMain,
      );
    } else {
      showError(result.toString());
    }
  }

  void _checkAuthResult(String result) {
    if (result == Error.doesNotExist) {
      Get.offAllNamed(AppRoutes.authenticatedBar);
    } else {
      showError(result);
    }
  }

  void showError(String result) {
    Get.snackbar(
      'Error',
      result,
      backgroundColor: kThemeController.errorMain,
      colorText: kThemeController.errorOnMain,
      snackPosition: SnackPosition.BOTTOM,
      margin: const EdgeInsets.all(10.0),
    );
  }

  void toSignIn() {
    loginFormKey = GlobalKey<FormState>();
    emailController.clear();
    passwordController.clear();
    Get.toNamed(AppRoutes.auth + AppRoutes.login);
  }

  void toSignUp() {
    registerFormKey = GlobalKey<FormState>();
    emailController.clear();
    passwordController.clear();
    pwConfirmationController.clear();
    Get.toNamed(AppRoutes.auth + AppRoutes.register);
  }

  void toResetPassword() {
    Get.toNamed(AppRoutes.auth + AppRoutes.resetPassword);
  }
}
