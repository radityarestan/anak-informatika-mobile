import 'package:anak_informatika/app/modules/authentication/authentication.dart';
import 'package:anak_informatika/app/shared/devices/size.dart';
import 'package:anak_informatika/app/shared/theme/theme.dart';
import 'package:anak_informatika/app/shared/widgets/widgets.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginView extends GetView<AuthenticationController> {
  const LoginView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size().init(context, true, false);

    return GetBuilder<ThemeController>(
      builder: (themeController) => Scaffold(
        backgroundColor: themeController.neutralBackground,
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          leading: BackButton(color: themeController.neutralOnBackground),
          backgroundColor: Colors.transparent,
          foregroundColor: themeController.neutralOutline,
          elevation: 0,
        ),
        body: Padding(
          padding: EdgeInsets.symmetric(horizontal: Size.w(5)),
          child: Form(
            key: controller.loginFormKey,
            child: Column(
              children: [
                SizedBox(height: Size.h(2)),
                Align(
                  alignment: Alignment.centerLeft,
                  child: AutoSizeText(
                    'login_greeting'.tr,
                    maxLines: 1,
                    style: kTheme.textTheme.headline1!.copyWith(
                      color: themeController.neutralOnBackground,
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: AutoSizeText(
                    'login_greetingSubtitle'.tr,
                    textAlign: TextAlign.left,
                    maxLines: 1,
                    style: kTheme.textTheme.subtitle1!.copyWith(
                      color: themeController.neutralOutline,
                    ),
                  ),
                ),
                SizedBox(height: Size.h(5)),
                Input(
                  width: Size.w(90),
                  prefixIcon: const Icon(Icons.email_outlined),
                  inputField: 'input_email'.tr,
                  controller: controller.emailController,
                  validator: (email) {
                    return controller.validateMail(email);
                  },
                ),
                SizedBox(height: Size.h(4)),
                Input(
                  width: Size.w(90),
                  prefixIcon: const Icon(Icons.lock_outlined),
                  inputField: 'input_password'.tr,
                  controller: controller.passwordController,
                  obscureText: true,
                  validator: (password) {
                    return controller.validatePassword(password);
                  },
                ),
                SizedBox(height: Size.h(4)),
                SquaredButton(
                  title: 'button_signIn'.tr,
                  onTap: () async {
                    await controller.signInWithEmailAndPassword();
                  },
                  width: Size.w(90),
                ),
                SquaredButtonNoFill(
                  title: 'login_forgotPw'.tr,
                  onTap: () {
                    controller.toResetPassword();
                  },
                ),
                SizedBox(height: Size.h(4)),
                AutoSizeText(
                  'login_choices'.tr,
                  maxLines: 1,
                  style: kTheme.textTheme.bodyText1!.copyWith(
                    color: themeController.neutralOutline,
                  ),
                ),
                SizedBox(height: Size.h(4)),
                SocialButton(
                  assetName: 'assets/images/google.png',
                  title: 'button_signInGoogle'.tr,
                  width: Size.w(90),
                  onTap: () async {
                    await controller.authWithGoogle(isSignUp: false);
                  },
                ),
                if (GetPlatform.isIOS) SizedBox(height: Size.h(1)),
                if (GetPlatform.isIOS)
                  SocialButton(
                    assetName: 'assets/images/apple.svg',
                    title: 'button_signInApple'.tr,
                    width: Size.w(90),
                    onTap: () async {
                      await controller.authWithApple(isSignUp: false);
                    },
                  ),
                SizedBox(height: Size.h(10)),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    AutoSizeText(
                      'login_dontHaveAccount'.tr,
                      maxLines: 1,
                      style: kTheme.textTheme.bodyText1!.copyWith(
                        color: themeController.neutralOutline,
                      ),
                    ),
                    SquaredButtonNoFill(
                      title: 'button_signUp'.tr,
                      onTap: () {
                        controller.toSignUp();
                      },
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
