import 'package:anak_informatika/app/modules/authentication/authentication_controller.dart';
import 'package:anak_informatika/app/shared/theme/theme.dart';
import 'package:anak_informatika/app/shared/devices/size.dart';
import 'package:anak_informatika/app/shared/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ResetPasswordView extends GetView<AuthenticationController> {
  const ResetPasswordView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ThemeController>(
      builder: (themeController) => Scaffold(
        backgroundColor: themeController.neutralBackground,
        appBar: AppBar(
          title: Text('appName'.tr),
          leading: BackButton(color: themeController.neutralOnBackground),
          backgroundColor: Colors.transparent,
          foregroundColor: themeController.neutralOutline,
          elevation: 0,
        ),
        body: Padding(
          padding: const EdgeInsets.all(12),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Input(
                width: Size.w(100),
                prefixIcon: const Icon(Icons.email_outlined),
                labelText: 'input_email'.tr,
                inputField: 'input_email'.tr,
                controller: controller.emailController,
                validator: (email) {
                  debugPrint(email);
                  return controller.validateMail(email);
                },
              ),
              SizedBox(
                height: Size.h(2),
              ),
              const Spacer(),
              Align(
                alignment: Alignment.centerRight,
                child: SquaredButton(
                  title: 'button_resetPassword'.tr,
                  width: Size.w(100),
                  onTap: () {
                    controller.changePassword(controller.emailController.text);
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
