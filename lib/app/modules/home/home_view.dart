import 'package:anak_informatika/app/data/models.dart';
import 'package:anak_informatika/app/modules/home/home.dart';
import 'package:anak_informatika/app/shared/time.dart';
import 'package:anak_informatika/app/shared/devices/size.dart';
import 'package:anak_informatika/app/shared/theme/theme.dart';
import 'package:anak_informatika/app/shared/widgets/widgets.dart';
import 'package:auto_size_text/auto_size_text.dart';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Home extends GetView<HomeController> {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size().init(context, true, true);

    return GetBuilder<ThemeController>(
      builder: (_) => Scaffold(
        backgroundColor: kThemeController.neutralBackground,
        body: Padding(
          padding: EdgeInsets.symmetric(horizontal: Size.w(2)),
          child: Stack(
            children: [
              GetBuilder<HomeController>(
                builder: (_) => FutureBuilder<List<Post>>(
                  future: controller.getPosts(),
                  builder: (context, snapshot) {
                    switch (snapshot.connectionState) {
                      case ConnectionState.none:
                        return const Text('Connection not yet started');
                      case ConnectionState.active:
                        return const Text('Active');
                      case ConnectionState.waiting:
                        return _buildLoading();
                      case ConnectionState.done:
                        if (snapshot.hasError) {
                          return _buildErrorPage(
                              'errorPage_somethingWentWrong'.tr);
                        } else if (!snapshot.hasData) {
                          return _buildErrorPage('errorPage_noData'.tr);
                        }
                        return _buildPosts(snapshot);
                    }
                  },
                ),
              ),
              Align(
                alignment: const Alignment(0.8, 0.9),
                child: PenButton(
                  onPressed: () {
                    controller.toCreatePost();
                  },
                  width: Size.w(10),
                  height: Size.w(10),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _buildErrorPage(String errorDescription) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            width: Size.w(30),
            height: Size.w(30),
            child: Image.asset('assets/images/sorry.png'),
          ),
          SizedBox(height: Size.h(1)),
          Text(
            errorDescription,
            style: kTheme.textTheme.bodyText1!.copyWith(
              color: kThemeController.neutralOnBackground,
            ),
          ),
          SizedBox(height: Size.h(5)),
          SquaredButton(
            title: 'button_tryAgain'.tr,
            onTap: () {
              controller.refreshPages();
            },
          ),
        ],
      ),
    );
  }

  _buildLoading() {
    return ListView.builder(
      itemCount: 4,
      itemBuilder: (_, index) => const ShimmerPostCard(),
    );
  }

  _buildPosts(AsyncSnapshot<List<Post>> snapshot) {
    return RefreshIndicator(
      color: kThemeController.neutralOutline,
      onRefresh: () async {
        controller.refreshPages();
      },
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.fromLTRB(
                  Size.w(3), Size.h(2), Size.w(3), Size.h(1)),
              child: AutoSizeText(
                'homepage_subheading'.tr,
                maxLines: 1,
                style: kTheme.textTheme.subtitle1!.copyWith(
                  color: kThemeController.neutralOnBackground,
                ),
              ),
            ),
            ListView.builder(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: snapshot.data!.length,
              itemBuilder: (_, index) {
                Post post = snapshot.data![index];
                Profile profile = post.author!;

                return Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: Size.w(3), vertical: Size.h(1)),
                  child: PostCard(
                    title: post.title!,
                    content: post.content!,
                    numberOfComments: post.commentCount!,
                    numberOfLikes: post.likeCount!,
                    isLiked: post.isLiked!,
                    isBookmarked: post.isBookmarked!,
                    username: profile.username!,
                    postTime: Time.timeAgo(post.postTime!),
                    imageUrl: profile.imageUrl!,
                    onPostCardTap: () {
                      controller.toViewPost(post.id!, profile.username!);
                    },
                    onBookmarkTap: () {
                      if (post.isBookmarked!) {
                        controller.unbookmarkPost(post.id!);
                      } else {
                        controller.bookmarkPost(post.id!);
                      }
                    },
                    onLikeTap: () {
                      if (post.isLiked!) {
                        controller.unlikePost(post.id!);
                      } else {
                        controller.likePost(post.id!);
                      }
                    },
                    onCommentTap: () {
                      controller.toCommentMaker(post.id!, profile.username!);
                    },
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
