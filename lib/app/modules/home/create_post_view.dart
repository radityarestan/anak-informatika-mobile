import 'package:anak_informatika/app/modules/home/home.dart';
import 'package:anak_informatika/app/shared/devices/size.dart';
import 'package:anak_informatika/app/shared/theme/theme.dart';
import 'package:anak_informatika/app/shared/widgets/widgets.dart';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CreatePostView extends GetView<HomeController> {
  const CreatePostView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size().init(context, true, false);

    return GetBuilder<ThemeController>(
      builder: (_) => Scaffold(
          backgroundColor: kThemeController.neutralBackground,
          appBar: AppBar(
            leading: BackButton(color: kThemeController.primaryOnMain),
            backgroundColor: kThemeController.appBarColor,
            title: Text(
              'appName'.tr,
              style: kTheme.textTheme.subtitle1!.copyWith(
                color: kThemeController.primaryOnMain,
              ),
            ),
          ),
          body: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
              child: Form(
                key: controller.uploadFormKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    InputPost(
                      width: double.infinity,
                      controller: controller.titleController,
                      validator: controller.validateInputContent,
                      labelText: 'input_title'.tr,
                      inputField: 'input_titleDesc'.tr,
                    ),
                    SizedBox(height: Size.h(3)),
                    InputPost(
                      width: double.infinity,
                      controller: controller.contentController,
                      validator: controller.validateInputContent,
                      onChanged: (data) {
                        controller.changeContent(data);
                      },
                      labelText: 'input_content'.tr,
                      inputField: 'input_contentDesc'.tr,
                    ),
                    SizedBox(height: Size.h(3)),
                    Text(
                      'createPost_markdownDesc'.tr,
                      style: kTheme.textTheme.bodyText1!.copyWith(
                          color: kThemeController.neutralOnBackground,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: Size.h(1)),
                    Obx(() => MarkdownContent(
                          data: controller.contentStream.value,
                          color: kThemeController.neutralOnBackground,
                        )),
                    SizedBox(height: Size.h(1)),
                    Align(
                      alignment: Alignment.centerRight,
                      child: SquaredButton(
                        title: 'button_upload'.tr,
                        onTap: () async => controller.uploadPost(),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )),
    );
  }
}
