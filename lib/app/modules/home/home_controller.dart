import 'dart:async';

import 'package:anak_informatika/app/data/models.dart';
import 'package:anak_informatika/app/modules/bookmark/bookmark_controller.dart';
import 'package:anak_informatika/app/routes/routes.dart';
import 'package:anak_informatika/app/services/api_provider.dart';
import 'package:anak_informatika/app/shared/widgets/widgets.dart';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomeController extends GetxController {
  final GlobalKey<FormState> uploadFormKey = GlobalKey<FormState>();
  final _apiService = Get.find<ApiProvider>();

  late TextEditingController titleController;
  late TextEditingController contentController;

  RxString contentStream = ''.obs;

  @override
  onInit() async {
    super.onInit();
    titleController = TextEditingController();
    contentController = TextEditingController();
  }

  @override
  onClose() {
    super.onClose();
    titleController.dispose();
    contentController.dispose();
  }

  Future<List<Post>> getPosts() async {
    return await _apiService.getRecommendedPost();
  }

  Future<void> refreshPages() async {
    update();
  }

  Future<void> uploadPost() async {
    if (uploadFormKey.currentState!.validate()) {
      Get.dialog(const Loading());
      var title = titleController.text;
      var content = contentController.text;

      // Call API using _apiService
      Post post = Post(
        title: title,
        content: content,
      );

      await _apiService.createPost(post);

      titleController.clear();
      contentController.clear();
      contentStream.value = '';

      await Future.delayed(const Duration(seconds: 3), () async {
        Get.back();
      });
      Get.back();
      await refreshPages();
    }
  }

  Future<void> likePost(String postId) async {
    await _apiService.likePostById(postId);
  }

  Future<void> unlikePost(String postId) async {
    await _apiService.unlikePostById(postId);
  }

  Future<void> bookmarkPost(String postId) async {
    await _apiService.bookmarkPostById(postId);
  }

  Future<void> unbookmarkPost(String postId) async {
    await _apiService.unbookmarkPostById(postId);
  }

  String? validateInputContent(String? content) {
    if (content!.isEmpty) {
      return 'error_emptyField'.tr;
    }
    return null;
  }

  void changeContent(String data) {
    contentStream.value = data;
  }

  void toCreatePost() {
    Get.toNamed(AppRoutes.home + AppRoutes.createPost);
  }

  void toViewPost(String postId, String authorname) {
    Get.toNamed(AppRoutes.viewPost, arguments: [postId, authorname]);
  }

  void toCommentMaker(String postId, String authorname) {
    Get.toNamed(AppRoutes.viewPost + AppRoutes.createComent,
        arguments: [postId, authorname]);
  }
}
