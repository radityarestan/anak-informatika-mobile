// ignore_for_file: must_be_immutable

import 'package:anak_informatika/app/data/models.dart';
import 'package:anak_informatika/app/modules/post/post_controller.dart';
import 'package:anak_informatika/app/shared/devices/size.dart';
import 'package:anak_informatika/app/shared/theme/theme.dart';
import 'package:anak_informatika/app/shared/time.dart';
import 'package:anak_informatika/app/shared/widgets/widgets.dart';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PostView extends GetView<PostController> {
  final String postId = Get.arguments[0];
  final String authorname = Get.arguments[1];

  PostView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size().init(context, true, false);

    return GetBuilder<ThemeController>(
      builder: (_) => Scaffold(
        backgroundColor: kThemeController.neutralBackground,
        appBar: AppBar(
          leading: BackButton(color: kThemeController.primaryOnMain),
          backgroundColor: kThemeController.appBarColor,
          title: Text(
            'appName'.tr,
            style: kTheme.textTheme.subtitle1!.copyWith(
              color: kThemeController.primaryOnMain,
            ),
          ),
        ),
        body: Padding(
          padding:
              EdgeInsets.symmetric(horizontal: Size.w(5), vertical: Size.h(1)),
          child: Column(
            children: [
              Expanded(
                child: GetBuilder<PostController>(
                  builder: (_) => FutureBuilder<Post>(
                      future: controller.getPost(postId),
                      builder: (_, snapshot) {
                        switch (snapshot.connectionState) {
                          case ConnectionState.none:
                            return const Text('Connection not yet started');
                          case ConnectionState.active:
                            return const Text('Active');
                          case ConnectionState.waiting:
                            return const Loading();
                          case ConnectionState.done:
                            if (snapshot.hasError) {
                              return const Text('Error');
                            } else if (!snapshot.hasData) {
                              return const Text('No Data');
                            }
                            return _buildPostDetail(snapshot.data!);
                        }
                      }),
                ),
              ),
              SquaredButton(
                title: 'button_createComment'.tr,
                width: double.infinity,
                onTap: () {
                  controller.goToCommentMaker(postId, authorname);
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  _buildPostDetail(Post post) {
    return RefreshIndicator(
      onRefresh: () async {
        controller.refreshPages();
      },
      child: SingleChildScrollView(
        physics: const ScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            PostCard(
              title: post.title!,
              content: post.content!,
              isLiked: post.isLiked!,
              isBookmarked: post.isBookmarked!,
              numberOfComments: post.commentCount!,
              numberOfLikes: post.likeCount!,
              username: post.author!.username!,
              postTime: Time.timeAgo(post.postTime!),
              imageUrl: post.author!.imageUrl!,
              onBookmarkTap: () {
                if (post.isBookmarked!) {
                  controller.unbookmarkPost(post.id!);
                } else {
                  controller.bookmarkPost(post.id!);
                }
              },
              onLikeTap: () {
                if (post.isLiked!) {
                  controller.unlikePost(post.id!);
                } else {
                  controller.likePost(post.id!);
                }
              },
              onCommentTap: () {
                controller.goToCommentMaker(post.id!, post.author!.username!);
              },
            ),
            SizedBox(height: Size.h(2)),
            Text(
              'Comments (${post.comments!.length.toString()})',
              style: kTheme.textTheme.subtitle1!
                  .copyWith(color: kThemeController.neutralOnBackground),
            ),
            ListView.builder(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: post.comments!.length,
              itemBuilder: (_, index) {
                Comment comment = post.comments![index];
                Profile profile = comment.author!;

                return Padding(
                  padding: EdgeInsets.symmetric(vertical: Size.h(1)),
                  child: CommentPostCard(
                    content: comment.content!,
                    username: profile.username!,
                    postTime: Time.timeAgo(comment.postTime!),
                    imageUrl: profile.imageUrl!,
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
