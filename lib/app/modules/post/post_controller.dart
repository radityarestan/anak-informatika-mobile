import 'package:anak_informatika/app/data/models.dart';
import 'package:anak_informatika/app/modules/home/home_controller.dart';
import 'package:anak_informatika/app/routes/routes.dart';
import 'package:anak_informatika/app/services/api_provider.dart';
import 'package:anak_informatika/app/shared/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PostController extends GetxController {
  final _homeController = Get.find<HomeController>();

  final GlobalKey<FormState> commentFormKey = GlobalKey<FormState>();
  final _apiService = Get.find<ApiProvider>();

  late TextEditingController commentController;

  @override
  void onInit() {
    super.onInit();
    commentController = TextEditingController();
  }

  @override
  void onClose() {
    super.onClose();
    commentController.dispose();
  }

  Future<void> refreshPages() async {
    update();
  }

  Future<void> likePost(String postId) async {
    await _apiService.likePostById(postId);
    await _homeController.refreshPages();
  }

  Future<void> unlikePost(String postId) async {
    await _apiService.unlikePostById(postId);
    await _homeController.refreshPages();
  }

  Future<void> bookmarkPost(String postId) async {
    await _apiService.bookmarkPostById(postId);
    await _homeController.refreshPages();
  }

  Future<void> unbookmarkPost(String postId) async {
    await _apiService.unbookmarkPostById(postId);
    await _homeController.refreshPages();
  }

  void goToCommentMaker(String postId, String authorname) {
    Get.toNamed(AppRoutes.viewPost + AppRoutes.createComent,
        arguments: [postId, authorname]);
  }

  String? validateInputContent(String? content) {
    if (content!.isEmpty) {
      return 'error_emptyField'.tr;
    }
    return null;
  }

  Future<Post> getPost(String id) async {
    return await _apiService.getPostById(id);
  }

  Future<void> createComment(String postId) async {
    if (commentFormKey.currentState!.validate()) {
      Get.dialog(const Loading());
      var comment = commentController.text;
      _apiService.createComment(postId, comment);
      await Future.delayed(const Duration(seconds: 3), () async {
        commentController.clear();
        Get.back();
      });
      Get.back();
      await refreshPages();
      await _homeController.refreshPages();
    }
  }
}
