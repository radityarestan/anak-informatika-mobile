import 'package:anak_informatika/app/modules/post/post.dart';
import 'package:anak_informatika/app/shared/devices/size.dart';
import 'package:anak_informatika/app/shared/theme/constant.dart';
import 'package:anak_informatika/app/shared/theme/theme_controller.dart';
import 'package:anak_informatika/app/shared/widgets/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class CommentMaker extends GetView<PostController> {
  final String postId = Get.arguments[0];
  final String author = Get.arguments[1];
  CommentMaker({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChannels.textInput.invokeMethod('TextInput.show');

    return GetBuilder<ThemeController>(
      builder: (_) => Scaffold(
        backgroundColor: kThemeController.neutralBackground,
        appBar: AppBar(
          leading: BackButton(color: kThemeController.primaryOnMain),
          backgroundColor: kThemeController.appBarColor,
          title: Text(
            'appName'.tr,
            style: kTheme.textTheme.subtitle1!.copyWith(
              color: kThemeController.primaryOnMain,
            ),
          ),
        ),
        body: Padding(
          padding:
              EdgeInsets.symmetric(horizontal: Size.w(5), vertical: Size.h(1)),
          child: SingleChildScrollView(
            child: Form(
              key: controller.commentFormKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  RichText(
                    text: TextSpan(
                      children: <TextSpan>[
                        TextSpan(
                          text: 'commentMaker_replyTo'.tr,
                          style: kTheme.textTheme.bodyText1!.copyWith(
                            color: kThemeController.neutralOnBackground,
                          ),
                        ),
                        TextSpan(
                          text: '@$author',
                          style: kTheme.textTheme.bodyText1!.copyWith(
                            color: kThemeController.primaryMain,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: Size.h(1)),
                  InputPost(
                    autofocus: true,
                    width: double.infinity,
                    controller: controller.commentController,
                    validator: controller.validateInputContent,
                    inputField: 'input_comment'.tr,
                  ),
                  SizedBox(height: Size.h(1)),
                  Align(
                    alignment: Alignment.centerRight,
                    child: SquaredButton(
                      onTap: () {
                        controller.createComment(postId);
                      },
                      title: 'button_upload'.tr,
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
