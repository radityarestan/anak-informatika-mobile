part of 'app_pages.dart';

class AppRoutes {
  static const splash = '/';

  static const auth = '/authentication';
  static const login = '/login';
  static const register = '/register';
  static const resetPassword = '/reset-password';

  static const authenticatedBar = '/bar';

  static const home = '/home';
  static const createPost = '/create-post/';
  static const createComent = '/create-comment';

  static const viewPost = '/view-post';

  static const bookmark = '/bookmark';

  static const settings = '/settings';
  static const profile = '/profile';
  static const about = '/about';
  static const translation = '/translation';
}
