import 'package:anak_informatika/app/modules/authentication/authentication.dart';
import 'package:anak_informatika/app/modules/bar/bar.dart';
import 'package:anak_informatika/app/modules/bookmark/bookmark_view.dart';
import 'package:anak_informatika/app/modules/home/home.dart';
import 'package:anak_informatika/app/modules/post/post.dart';
import 'package:anak_informatika/app/modules/settings/settings.dart';
import 'package:anak_informatika/app/modules/splash/splash.dart';

import 'package:get/route_manager.dart';

part 'app_routes.dart';

class AppPages {
  static const initial = AppRoutes.splash;

  static final routes = [
    GetPage(
      name: AppRoutes.splash,
      page: () => const SplashView(),
      binding: SplashBinding(),
    ),
    GetPage(
      name: AppRoutes.auth,
      page: () => const AuthenticationView(),
      binding: AuthenticationBinding(),
      children: [
        GetPage(
          name: AppRoutes.login,
          page: () => const LoginView(),
        ),
        GetPage(
          name: AppRoutes.register,
          page: () => const RegisterView(),
        ),
        GetPage(
          name: AppRoutes.resetPassword,
          page: () => const ResetPasswordView(),
        ),
      ],
    ),
    GetPage(
      name: AppRoutes.authenticatedBar,
      page: () => const AuthenticatedBar(),
      binding: BarBinding(),
    ),
    GetPage(
      name: AppRoutes.home,
      page: () => const Home(),
      children: [
        GetPage(
          name: AppRoutes.createPost,
          page: () => const CreatePostView(),
        ),
      ],
    ),
    GetPage(
      name: AppRoutes.viewPost,
      page: () => PostView(),
      children: [
        GetPage(
          name: AppRoutes.createComent,
          page: () => CommentMaker(),
        ),
      ],
    ),
    GetPage(
      name: AppRoutes.bookmark,
      page: () => const Bookmark(),
    ),
    GetPage(
      name: AppRoutes.settings,
      page: () => const SettingsView(),
      children: [
        GetPage(
          name: AppRoutes.profile,
          page: () => ProfileSettings(),
        ),
        GetPage(
          name: AppRoutes.translation,
          page: () => const TranslationView(),
        ),
        GetPage(
          name: AppRoutes.about,
          page: () => const About(),
        ),
      ],
    ),
  ];
}
