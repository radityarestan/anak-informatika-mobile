class Keys {
  static const isAlreadySignedIn = 'isAlreadySignedIn';
  static const isDarkMode = 'isDarkMode';
  static const languageCode = 'langCode';
}
