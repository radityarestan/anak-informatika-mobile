import 'package:anak_informatika/app/shared/keys.dart';
import 'package:anak_informatika/app/shared/theme/color/color.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class ThemeController extends GetxController {
  final _box = GetStorage();
  final _key = Keys.isDarkMode;

  var white = ColorPallete.white;
  var black = ColorPallete.black;
  var transparent = ColorPallete.transparent;
  var linkColor = ColorPallete.blue700;

  var appBarColor = LightModeColor.primaryMain;

  var primaryMain = LightModeColor.primaryMain;
  var primaryOnMain = LightModeColor.primaryOnMain;
  var primaryContainer = LightModeColor.primaryContainer;
  var primaryOnContainer = LightModeColor.primaryOnContainer;

  var successMain = LightModeColor.successMain;
  var successOnMain = LightModeColor.successOnMain;
  var successContainer = LightModeColor.successContainer;
  var successOnContainer = LightModeColor.successOnContainer;

  var warningMain = LightModeColor.warningMain;
  var warningOnMain = LightModeColor.warningOnMain;
  var warningContainer = LightModeColor.warningContainer;
  var warningOnContainer = LightModeColor.warningOnContainer;

  var errorMain = LightModeColor.errorMain;
  var errorOnMain = LightModeColor.errorOnMain;
  var errorContainer = LightModeColor.errorContainer;
  var errorOnContainer = LightModeColor.errorOnContainer;

  var neutralOutline = LightModeColor.neutralOutline;
  var neutralBorder = LightModeColor.neutralBorder;
  var neutralBackground = LightModeColor.neutralBackground;
  var neutralOnBackground = LightModeColor.neutralOnBackground;
  var neutralSurface = LightModeColor.neutralSurface;
  var neutralSurface2 = LightModeColor.neutralSurface2;
  var neutralOnSurface = LightModeColor.neutralOnSurface;

  @override
  void onInit() {
    super.onInit();
    var _isDarkMode = _box.read(_key) ?? false;
    _updateColor(_isDarkMode);
  }

  void saveTheme(bool isDarkMode) async {
    await _box.write(_key, isDarkMode);
    _updateColor(isDarkMode);
  }

  void _updateColor(bool isDarkMode) {
    if (isDarkMode) {
      _updateToDarkMode();
    } else {
      _updateToLightMode();
    }
    update();
  }

  void _updateToLightMode() {
    appBarColor = LightModeColor.primaryMain;

    primaryMain = LightModeColor.primaryMain;
    primaryOnMain = LightModeColor.primaryOnMain;
    primaryContainer = LightModeColor.primaryContainer;
    primaryOnContainer = LightModeColor.primaryOnContainer;

    successMain = LightModeColor.successMain;
    successOnMain = LightModeColor.successOnMain;
    successContainer = LightModeColor.successContainer;
    successOnContainer = LightModeColor.successOnContainer;

    warningMain = LightModeColor.warningMain;
    warningOnMain = LightModeColor.warningOnMain;
    warningContainer = LightModeColor.warningContainer;
    warningOnContainer = LightModeColor.warningOnContainer;

    errorMain = LightModeColor.errorMain;
    errorOnMain = LightModeColor.errorOnMain;
    errorContainer = LightModeColor.errorContainer;
    errorOnContainer = LightModeColor.errorOnContainer;

    neutralOutline = LightModeColor.neutralOutline;
    neutralBorder = LightModeColor.neutralBorder;
    neutralBackground = LightModeColor.neutralBackground;
    neutralOnBackground = LightModeColor.neutralOnBackground;
    neutralSurface = LightModeColor.neutralSurface;
    neutralSurface2 = LightModeColor.neutralSurface2;
    neutralOnSurface = LightModeColor.neutralOnSurface;
  }

  void _updateToDarkMode() {
    appBarColor = DarkModeColor.neutralSurface2;

    primaryMain = DarkModeColor.primaryMain;
    primaryOnMain = DarkModeColor.primaryOnMain;
    primaryContainer = DarkModeColor.primaryContainer;
    primaryOnContainer = DarkModeColor.primaryOnContainer;

    successMain = DarkModeColor.successMain;
    successOnMain = DarkModeColor.successOnMain;
    successContainer = DarkModeColor.successContainer;
    successOnContainer = DarkModeColor.successOnContainer;

    warningMain = DarkModeColor.warningMain;
    warningOnMain = DarkModeColor.warningOnMain;
    warningContainer = DarkModeColor.warningContainer;
    warningOnContainer = DarkModeColor.warningOnContainer;

    errorMain = DarkModeColor.errorMain;
    errorOnMain = DarkModeColor.errorOnMain;
    errorContainer = DarkModeColor.errorContainer;
    errorOnContainer = DarkModeColor.errorOnContainer;

    neutralOutline = DarkModeColor.neutralOutline;
    neutralBorder = DarkModeColor.neutralBorder;
    neutralBackground = DarkModeColor.neutralBackground;
    neutralOnBackground = DarkModeColor.neutralOnBackground;
    neutralSurface = DarkModeColor.neutralSurface;
    neutralSurface2 = DarkModeColor.neutralSurface2;
    neutralOnSurface = DarkModeColor.neutralOnSurface;
  }
}
