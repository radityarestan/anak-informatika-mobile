import 'package:anak_informatika/app/shared/theme/theme.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

final kThemeController = Get.find<ThemeController>();

final kTheme = ThemeData(
  textTheme: TextTheme(
    headline1: GoogleFonts.roboto(
      color: kThemeController.neutralOnBackground,
      fontSize: 36.0,
    ),
    headline2: GoogleFonts.roboto(
      color: kThemeController.neutralOnBackground,
      fontSize: 32.0,
    ),
    headline3: GoogleFonts.roboto(
      color: kThemeController.neutralOnBackground,
      fontSize: 28.0,
    ),
    headline4: GoogleFonts.roboto(
      color: kThemeController.neutralOnBackground,
      fontSize: 24.0,
    ),
    headline5: GoogleFonts.roboto(
      color: kThemeController.neutralOnBackground,
      fontSize: 22.0,
    ),
    subtitle1: GoogleFonts.roboto(
      color: kThemeController.neutralOnBackground,
      fontSize: 18.0,
    ),
    // substitle2 will be used for body large
    subtitle2: GoogleFonts.roboto(
      color: kThemeController.neutralOnBackground,
      fontSize: 16.0,
    ),
    // bodyText1 will be used for body regular
    bodyText1: GoogleFonts.roboto(
      color: kThemeController.neutralOnBackground,
      fontSize: 14.0,
    ),
    // bodyText2 will be used for body small
    bodyText2: GoogleFonts.roboto(
      color: kThemeController.neutralOnBackground,
      fontSize: 12.0,
    ),
    caption: GoogleFonts.roboto(
      color: kThemeController.neutralOnBackground,
      fontSize: 11.0,
    ),
    // overline will be used for footer
    overline: GoogleFonts.roboto(
      color: kThemeController.neutralOnBackground,
      fontSize: 10.0,
    ),
  ),
);
