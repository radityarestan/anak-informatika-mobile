import 'package:flutter/material.dart';

class ColorPallete {
  static const white = Colors.white;
  static const black = Colors.black;
  static const transparent = Colors.transparent;

  static const neutral50 = Color(0xFFFAFAFA);
  static const neutral100 = Color(0xFFF5F5F5);
  static const neutral200 = Color(0xFFE5E5E5);
  static const neutral300 = Color(0xFFD4D4D4);
  static const neutral400 = Color(0xFFA3A3A3);
  static const neutral500 = Color(0xFF737373);
  static const neutral600 = Color(0xFF525252);
  static const neutral700 = Color(0xFF404040);
  static const neutral800 = Color(0xFF262626);
  static const neutral900 = Color(0xFF171717);

  static const neutral50Surface2 = Color(0xFFECECEC);
  static const neutral900Surface2 = Color(0xFF232323);

  static const primary50 = Color(0xFFFBE9E7);
  static const primary100 = Color(0xFFFFCCBC);
  static const primary200 = Color(0xFFFFAB91);
  static const primary300 = Color(0xFFFF8A65);
  static const primary400 = Color(0xFFFF7043);
  static const primary500 = Color(0xFFFF5722);
  static const primary600 = Color(0xFFF4511E);
  static const primary700 = Color(0xFFE64A19);
  static const primary800 = Color(0xFFD84315);
  static const primary900 = Color(0xFFBF360C);

  static const success50 = Color(0xFFE8F5E9);
  static const success100 = Color(0xFFC8E6C9);
  static const success200 = Color(0xFFA5D6A7);
  static const success300 = Color(0xFF81C784);
  static const success400 = Color(0xFF66BB6A);
  static const success500 = Color(0xFF4CAF50);
  static const success600 = Color(0xFF43A047);
  static const success700 = Color(0xFF388E3C);
  static const success800 = Color(0xFF2E7D32);
  static const success900 = Color(0xFF1B5320);

  static const warning50 = Color(0xFFFFF3E0);
  static const warning100 = Color(0xFFFFE0B2);
  static const warning200 = Color(0xFFFFCC80);
  static const warning300 = Color(0xFFFFB74D);
  static const warning400 = Color(0xFFFFA726);
  static const warning500 = Color(0xFFFF9800);
  static const warning600 = Color(0xFFFB8C00);
  static const warning700 = Color(0xFFF57C00);
  static const warning800 = Color(0xFFEF6C00);
  static const warning900 = Color(0xFFE65100);

  static const error50 = Color(0xFFFFEBEE);
  static const error100 = Color(0xFFFFCDD2);
  static const error200 = Color(0xFFEF9A9A);
  static const error300 = Color(0xFFE57373);
  static const error400 = Color(0xFFEF5350);
  static const error500 = Color(0xFFF44336);
  static const error600 = Color(0xFFE53935);
  static const error700 = Color(0xFFD32F2F);
  static const error800 = Color(0xFFC62828);
  static const error900 = Color(0xFFB71C1C);

  static const blue700 = Color(0xFF2962FF);
}
