import 'package:anak_informatika/app/shared/theme/color/color.dart';

class LightModeColor {
  static const primaryMain = ColorPallete.primary600;
  static const primaryOnMain = ColorPallete.white;
  static const primaryContainer = ColorPallete.primary100;
  static const primaryOnContainer = ColorPallete.primary900;

  static const successMain = ColorPallete.success600;
  static const successOnMain = ColorPallete.white;
  static const successContainer = ColorPallete.success100;
  static const successOnContainer = ColorPallete.success900;

  static const warningMain = ColorPallete.warning600;
  static const warningOnMain = ColorPallete.white;
  static const warningContainer = ColorPallete.warning100;
  static const warningOnContainer = ColorPallete.warning900;

  static const errorMain = ColorPallete.error600;
  static const errorOnMain = ColorPallete.white;
  static const errorContainer = ColorPallete.error100;
  static const errorOnContainer = ColorPallete.error900;

  static const neutralOutline = ColorPallete.neutral500;
  static const neutralBorder = ColorPallete.neutral300;
  static const neutralBackground = ColorPallete.neutral50;
  static const neutralOnBackground = ColorPallete.neutral900;
  static const neutralSurface = ColorPallete.neutral50;
  static const neutralSurface2 = ColorPallete.neutral50Surface2;
  static const neutralOnSurface = ColorPallete.neutral900;
}
