import 'package:anak_informatika/app/shared/theme/color/color.dart';

class DarkModeColor {
  static const primaryMain = ColorPallete.primary400;
  static const primaryOnMain = ColorPallete.neutral100;
  static const primaryContainer = ColorPallete.primary700;
  static const primaryOnContainer = ColorPallete.primary100;

  static const successMain = ColorPallete.success400;
  static const successOnMain = ColorPallete.neutral100;
  static const successContainer = ColorPallete.success700;
  static const successOnContainer = ColorPallete.success100;

  static const warningMain = ColorPallete.warning400;
  static const warningOnMain = ColorPallete.neutral100;
  static const warningContainer = ColorPallete.warning700;
  static const warningOnContainer = ColorPallete.warning100;

  static const errorMain = ColorPallete.error400;
  static const errorOnMain = ColorPallete.neutral100;
  static const errorContainer = ColorPallete.error700;
  static const errorOnContainer = ColorPallete.error100;

  static const neutralOutline = ColorPallete.neutral400;
  static const neutralBorder = ColorPallete.neutral700;
  static const neutralBackground = ColorPallete.neutral900;
  static const neutralOnBackground = ColorPallete.neutral100;
  static const neutralSurface = ColorPallete.neutral900;
  static const neutralSurface2 = ColorPallete.neutral900Surface2;
  static const neutralOnSurface = ColorPallete.neutral200;
}
