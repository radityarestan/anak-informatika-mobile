import 'package:get/get.dart';

class Time {
  static String timeAgo(DateTime dateTime) {
    Duration diff = DateTime.now().difference(dateTime);

    String result = 'postcard_dateNow'.tr;
    if (diff.inDays > 365) {
      result =
          "${(diff.inDays / 365).floor()} ${(diff.inDays / 365).floor() == 1 ? "postcard_dateYear".tr : "postcard_dateYears".tr}";
    } else if (diff.inDays > 30) {
      result =
          "${(diff.inDays / 30).floor()} ${(diff.inDays / 30).floor() == 1 ? "postcard_dateMonth".tr : "postcard_dateMonths".tr}";
    } else if (diff.inDays > 7) {
      result =
          "${(diff.inDays / 7).floor()} ${(diff.inDays / 7).floor() == 1 ? "postcard_dateWeek".tr : "postcard_dateWeeks".tr}";
    } else if (diff.inDays > 0) {
      result =
          "${diff.inDays} ${diff.inDays == 1 ? "postcard_dateDay".tr : "postcard_dateDays".tr}";
    } else if (diff.inHours > 0) {
      result =
          "${diff.inHours} ${diff.inHours == 1 ? "postcard_dateHours".tr : "postcard_dateHours".tr}";
    } else if (diff.inMinutes > 0) {
      result =
          "${diff.inMinutes} ${diff.inMinutes == 1 ? "postcard_dateMinute".tr : "postcard_dateMinutes".tr}";
    }

    return result;
  }
}
