import 'package:flutter/material.dart';

class Size {
  static late double width;
  static late double height;

  void init(BuildContext context, bool appBar, bool navBar) {
    MediaQueryData mediaQuery = MediaQuery.of(context);

    var screenWidth = mediaQuery.size.width;
    var screenHeight = mediaQuery.size.height;

    var safeWidth =
        screenWidth - mediaQuery.padding.left - mediaQuery.padding.right;
    var safeHeight =
        screenHeight - mediaQuery.padding.top - mediaQuery.padding.bottom;

    if (appBar && navBar) {
      safeHeight = safeHeight - 2 * AppBar().preferredSize.height;
    } else if (appBar || navBar) {
      safeHeight = safeHeight - AppBar().preferredSize.height;
    }

    width = safeWidth / 100;
    height = safeHeight / 100;
  }

  static double w(double percent) => width * percent;
  static double h(double percent) => height * percent;
}
