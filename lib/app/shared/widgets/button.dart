import 'package:flutter/material.dart';
import 'package:anak_informatika/app/shared/theme/theme.dart';
import 'package:flutter_svg/svg.dart';

class SquaredButton extends StatelessWidget {
  final IconData? prefixIcon;
  final IconData? suffixIcon;
  final Function()? onTap;
  final bool disabled;
  final double? width;
  final String title;

  const SquaredButton({
    Key? key,
    required this.title,
    this.onTap,
    this.width,
    this.prefixIcon,
    this.suffixIcon,
    this.disabled = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Color buttonColor = kThemeController.primaryMain;
    Color contentColor = kThemeController.white;
    Color splashColor = kThemeController.primaryMain.withOpacity(0.16);

    if (disabled) {
      buttonColor = kThemeController.neutralOnSurface.withOpacity(0.12);
      contentColor = kThemeController.neutralOnSurface.withOpacity(0.6);
      splashColor = kThemeController.transparent;
    }

    return SizedBox(
      width: width,
      child: Material(
        color: buttonColor,
        borderRadius: BorderRadius.circular(6.0),
        child: InkWell(
          onTap: onTap,
          borderRadius: BorderRadius.circular(6.0),
          splashColor: splashColor,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(16, 12, 16, 12),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                if (prefixIcon != null)
                  Icon(
                    prefixIcon,
                    color: contentColor,
                    size: 20.0,
                  ),
                Text(
                  title,
                  textAlign: TextAlign.center,
                  style: kTheme.textTheme.bodyText1!.copyWith(
                    color: contentColor,
                  ),
                ),
                if (suffixIcon != null)
                  Icon(
                    suffixIcon,
                    color: contentColor,
                    size: 20.0,
                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class RoundedButton extends StatelessWidget {
  final IconData? prefixIcon;
  final IconData? suffixIcon;
  final Function()? onTap;
  final bool disabled;
  final double? width;
  final String title;

  const RoundedButton({
    Key? key,
    required this.title,
    this.onTap,
    this.width,
    this.prefixIcon,
    this.suffixIcon,
    this.disabled = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Color buttonColor = kThemeController.primaryMain;
    Color contentColor = kThemeController.white;
    Color splashColor = kThemeController.primaryMain.withOpacity(0.16);

    if (disabled) {
      buttonColor = kThemeController.neutralOnSurface.withOpacity(0.12);
      contentColor = kThemeController.neutralOnSurface.withOpacity(0.6);
      splashColor = kThemeController.transparent;
    }

    return SizedBox(
      width: width,
      child: Material(
        color: buttonColor,
        borderRadius: BorderRadius.circular(30.0),
        child: InkWell(
          onTap: onTap,
          borderRadius: BorderRadius.circular(30.0),
          splashColor: splashColor,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(16, 12, 16, 12),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                if (prefixIcon != null)
                  Icon(
                    prefixIcon,
                    color: contentColor,
                    size: 20.0,
                  ),
                Text(
                  title,
                  textAlign: TextAlign.center,
                  style: kTheme.textTheme.bodyText1!.copyWith(
                    color: contentColor,
                  ),
                ),
                if (suffixIcon != null)
                  Icon(
                    suffixIcon,
                    color: contentColor,
                    size: 20.0,
                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class OutlineSquaredButton extends StatelessWidget {
  final IconData? prefixIcon;
  final IconData? suffixIcon;
  final Function()? onTap;
  final bool disabled;
  final double? width;
  final String title;

  const OutlineSquaredButton({
    Key? key,
    required this.title,
    this.width,
    this.onTap,
    this.prefixIcon,
    this.suffixIcon,
    this.disabled = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Color buttonColor = kThemeController.transparent;
    Color mainColor = kThemeController.primaryMain;
    Color splashColor = kThemeController.primaryMain.withOpacity(0.16);

    if (disabled) {
      mainColor = kThemeController.neutralOnSurface.withOpacity(0.40);
      splashColor = kThemeController.transparent;
    }

    return Container(
      width: width,
      decoration: BoxDecoration(
        border: Border.all(
          width: 1.0,
          color: mainColor,
        ),
        borderRadius: BorderRadius.circular(6.0),
      ),
      child: Material(
        borderRadius: BorderRadius.circular(6.0),
        color: buttonColor,
        child: InkWell(
          onTap: onTap,
          borderRadius: BorderRadius.circular(6.0),
          splashColor: splashColor,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(16, 12, 16, 12),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                if (prefixIcon != null)
                  Icon(
                    prefixIcon,
                    color: mainColor,
                    size: 20.0,
                  ),
                Text(
                  title,
                  textAlign: TextAlign.center,
                  style: kTheme.textTheme.bodyText1!.copyWith(
                    color: mainColor,
                  ),
                ),
                if (suffixIcon != null)
                  Icon(
                    suffixIcon,
                    color: mainColor,
                    size: 20.0,
                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class RoundedOutlineButton extends StatelessWidget {
  final IconData? prefixIcon;
  final IconData? suffixIcon;
  final Function()? onTap;
  final bool disabled;
  final double? width;
  final String title;

  const RoundedOutlineButton({
    Key? key,
    required this.title,
    this.width,
    this.onTap,
    this.prefixIcon,
    this.suffixIcon,
    this.disabled = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Color buttonColor = kThemeController.transparent;
    Color mainColor = kThemeController.primaryMain;
    Color splashColor = kThemeController.primaryMain.withOpacity(0.16);

    if (disabled) {
      mainColor = kThemeController.neutralOnSurface.withOpacity(0.40);
      splashColor = kThemeController.transparent;
    }

    return Container(
      width: width,
      decoration: BoxDecoration(
        border: Border.all(
          width: 1.0,
          color: mainColor,
        ),
        borderRadius: BorderRadius.circular(30.0),
      ),
      child: Material(
        borderRadius: BorderRadius.circular(30.0),
        color: buttonColor,
        child: InkWell(
          onTap: onTap,
          borderRadius: BorderRadius.circular(30.0),
          splashColor: splashColor,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(16, 12, 16, 12),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                if (prefixIcon != null)
                  Icon(
                    prefixIcon,
                    color: mainColor,
                    size: 20.0,
                  ),
                Text(
                  title,
                  textAlign: TextAlign.center,
                  style: kTheme.textTheme.bodyText1!.copyWith(
                    color: mainColor,
                  ),
                ),
                if (suffixIcon != null)
                  Icon(
                    suffixIcon,
                    color: mainColor,
                    size: 20.0,
                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class SocialButton extends StatelessWidget {
  final Function()? onTap;
  final String assetName;
  final bool disabled;
  final double? width;
  final String? title;

  const SocialButton({
    Key? key,
    required this.assetName,
    this.title,
    this.width,
    this.onTap,
    this.disabled = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool isSVG = assetName.contains('svg');

    Color buttonColor = kThemeController.neutralSurface;
    Color contentColor = kThemeController.neutralOnSurface;
    Color splashColor = kThemeController.primaryMain.withOpacity(0.16);

    if (disabled) {
      buttonColor = kThemeController.neutralOnSurface.withOpacity(0.05);
      splashColor = kThemeController.transparent;
    }

    return Container(
      width: width,
      decoration: BoxDecoration(
        border: Border.all(
          width: 1.5,
          color: kThemeController.neutralOnSurface.withOpacity(0.12),
        ),
        borderRadius: BorderRadius.circular(6.0),
      ),
      child: Material(
        color: buttonColor,
        borderRadius: BorderRadius.circular(6.0),
        child: InkWell(
          onTap: onTap,
          borderRadius: BorderRadius.circular(6.0),
          splashColor: splashColor,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(16, 12, 16, 12),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                isSVG
                    ? SvgPicture.asset(
                        assetName,
                        width: 20.0,
                        height: 20.0,
                        color: kThemeController.neutralOnSurface,
                      )
                    : Image.asset(
                        assetName,
                        width: 20.0,
                        height: 20.0,
                      ),
                if (title != null)
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Text(
                      title!,
                      textAlign: TextAlign.center,
                      style: kTheme.textTheme.bodyText1!.copyWith(
                        color: contentColor,
                      ),
                    ),
                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class SquaredButtonNoFill extends StatelessWidget {
  final IconData? prefixIcon;
  final IconData? suffixIcon;
  final Function()? onTap;
  final bool disabled;
  final double? width;
  final String title;

  const SquaredButtonNoFill({
    Key? key,
    required this.title,
    this.onTap,
    this.width,
    this.prefixIcon,
    this.suffixIcon,
    this.disabled = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Color buttonColor = Colors.transparent;
    Color contentColor = kThemeController.primaryMain;
    Color splashColor = kThemeController.primaryMain.withOpacity(0.16);

    if (disabled) {
      buttonColor = kThemeController.neutralOnSurface.withOpacity(0.12);
      contentColor = kThemeController.neutralOnSurface.withOpacity(0.6);
      splashColor = kThemeController.transparent;
    }

    return SizedBox(
      width: width,
      child: Material(
        color: buttonColor,
        borderRadius: BorderRadius.circular(6.0),
        child: InkWell(
          onTap: onTap,
          borderRadius: BorderRadius.circular(6.0),
          splashColor: splashColor,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(8, 12, 8, 12),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                if (prefixIcon != null)
                  Icon(
                    prefixIcon,
                    color: contentColor,
                    size: 20.0,
                  ),
                Text(
                  title,
                  textAlign: TextAlign.center,
                  style: kTheme.textTheme.bodyText1!.copyWith(
                    color: contentColor,
                  ),
                ),
                if (suffixIcon != null)
                  Icon(
                    suffixIcon,
                    color: contentColor,
                    size: 20.0,
                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class PenButton extends StatelessWidget {
  final Function()? onPressed;
  final double width;
  final double height;

  const PenButton({
    Key? key,
    required this.onPressed,
    required this.width,
    required this.height,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: height,
      child: Material(
        color: kThemeController.primaryMain,
        borderRadius: BorderRadius.circular(width / 2),
        child: InkWell(
          onTap: onPressed,
          borderRadius: BorderRadius.circular(width / 2),
          child: Icon(
            Icons.create,
            color: kThemeController.white,
          ),
        ),
      ),
    );
  }
}

class CustomIconButton extends StatelessWidget {
  final IconData iconData;
  final double iconSize;
  final Function()? onTap;

  const CustomIconButton({
    Key? key,
    required this.iconData,
    required this.iconSize,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: kThemeController.transparent,
      borderRadius: BorderRadius.circular(2.0),
      child: InkWell(
        onTap: onTap,
        highlightColor: kThemeController.transparent,
        borderRadius: BorderRadius.circular(2.0),
        child: Padding(
          padding: const EdgeInsets.all(1.0),
          child: Icon(
            iconData,
            size: iconSize,
            color: kThemeController.neutralOnBackground,
          ),
        ),
      ),
    );
  }
}
