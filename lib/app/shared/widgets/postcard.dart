// ignore_for_file: must_be_immutable

import 'package:anak_informatika/app/shared/widgets/widgets.dart';
import 'package:anak_informatika/app/shared/devices/size.dart';
import 'package:anak_informatika/app/shared/theme/theme.dart';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';

class PostCard extends StatelessWidget {
  final String title;
  final String content;
  final int numberOfComments;
  final int numberOfLikes;
  final String username;
  final String postTime;
  final String imageUrl;
  final bool isLiked;
  final bool isBookmarked;
  final Function()? onBookmarkTap;
  final Function()? onLikeTap;
  final Function()? onCommentTap;
  final Function()? onPostCardTap;

  const PostCard({
    Key? key,
    required this.title,
    required this.content,
    required this.numberOfComments,
    required this.numberOfLikes,
    required this.username,
    required this.postTime,
    required this.imageUrl,
    required this.isLiked,
    required this.isBookmarked,
    required this.onBookmarkTap,
    required this.onLikeTap,
    required this.onCommentTap,
    this.onPostCardTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ThemeController>(
      builder: (_) => onPostCardTap != null
          ? ClickableCard(
              onTap: onPostCardTap,
              child: DetailPostCard(
                title: title,
                content: content,
                numberOfComments: numberOfComments,
                numberOfLikes: numberOfLikes,
                username: username,
                postTime: postTime,
                imageUrl: imageUrl,
                isLiked: isLiked,
                isBookmarked: isBookmarked,
                onLikeTap: onLikeTap,
                onCommentTap: onCommentTap,
                onBookmarkTap: onBookmarkTap,
              ),
            )
          : DetailPostCard(
              title: title,
              content: content,
              numberOfComments: numberOfComments,
              numberOfLikes: numberOfLikes,
              username: username,
              postTime: postTime,
              imageUrl: imageUrl,
              isLiked: isLiked,
              isBookmarked: isBookmarked,
              onLikeTap: onLikeTap,
              onCommentTap: onCommentTap,
              onBookmarkTap: onBookmarkTap,
            ),
    );
  }
}

class ClickableCard extends StatelessWidget {
  final Function()? onTap;
  final Widget child;

  const ClickableCard({
    Key? key,
    required this.child,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: kThemeController.neutralSurface2,
        borderRadius: BorderRadius.circular(20),
        boxShadow: [
          BoxShadow(
            color: kThemeController.black.withOpacity(0.5),
            spreadRadius: 0.5,
            blurRadius: 3,
            offset: const Offset(0, 1.5),
          )
        ],
      ),
      child: Material(
        color: kThemeController.neutralSurface2,
        borderRadius: BorderRadius.circular(20),
        child: InkWell(
          onTap: onTap,
          borderRadius: BorderRadius.circular(20),
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: child,
          ),
        ),
      ),
    );
  }
}

class DetailPostCard extends StatelessWidget {
  final String title;
  final String content;
  final int numberOfComments;
  final int numberOfLikes;
  final String username;
  final String postTime;
  final String imageUrl;
  final bool isLiked;
  final bool isBookmarked;
  final Function()? onBookmarkTap;
  final Function()? onLikeTap;
  final Function()? onCommentTap;

  const DetailPostCard({
    Key? key,
    required this.title,
    required this.content,
    required this.numberOfComments,
    required this.numberOfLikes,
    required this.username,
    required this.postTime,
    required this.imageUrl,
    required this.isLiked,
    required this.isBookmarked,
    required this.onBookmarkTap,
    required this.onLikeTap,
    required this.onCommentTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        PostCardHeader(
          imageUrl: imageUrl,
          username: username,
          postTime: postTime,
          isBookmarked: isBookmarked,
          onBookmarkTap: onBookmarkTap,
        ),
        SizedBox(height: Size.h(1)),
        Text(
          title,
          style: kTheme.textTheme.subtitle1!
              .copyWith(color: kThemeController.neutralOnBackground),
        ),
        SizedBox(height: Size.h(1)),
        MarkdownContent(
          data: content,
          color: kThemeController.neutralOutline,
        ),
        SizedBox(height: Size.h(1)),
        PostCardFooter(
          isLiked: isLiked,
          numberOfLikes: numberOfLikes,
          numberOfComments: numberOfComments,
          onLikeTap: onLikeTap,
          onCommentTap: onCommentTap,
        ),
      ],
    );
  }
}

class CommentPostCard extends StatelessWidget {
  final String content;
  final String username;
  final String postTime;
  final String imageUrl;

  const CommentPostCard({
    Key? key,
    required this.content,
    required this.username,
    required this.postTime,
    required this.imageUrl,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ThemeController>(
      builder: (_) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          PostCardHeader(
            imageUrl: imageUrl,
            username: username,
            postTime: postTime,
          ),
          SizedBox(height: Size.h(1)),
          MarkdownContent(
            data: content,
            color: kThemeController.neutralOnBackground,
          ),
        ],
      ),
    );
  }
}

class PostCardHeader extends StatefulWidget {
  final String imageUrl;
  final String username;
  final String postTime;
  final Function()? onBookmarkTap;

  bool? isBookmarked;

  PostCardHeader({
    Key? key,
    required this.imageUrl,
    required this.username,
    required this.postTime,
    this.isBookmarked,
    this.onBookmarkTap,
  }) : super(key: key);

  @override
  _PostCardHeaderState createState() => _PostCardHeaderState();
}

class _PostCardHeaderState extends State<PostCardHeader> {
  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CircleAvatar(
          radius: Size.w(5),
          backgroundImage: NetworkImage(widget.imageUrl),
        ),
        SizedBox(width: Size.w(2)),
        Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              width: Size.w(50),
              child: AutoSizeText(
                widget.username,
                overflow: TextOverflow.ellipsis,
                style: kTheme.textTheme.subtitle2!
                    .copyWith(color: kThemeController.neutralOnBackground),
              ),
            ),
            Text(
              widget.postTime,
              style: kTheme.textTheme.bodyText1!
                  .copyWith(color: kThemeController.neutralOutline),
            ),
          ],
        ),
        const Spacer(),
        if (widget.onBookmarkTap != null && widget.isBookmarked != null)
          CustomIconButton(
            onTap: () async {
              widget.isBookmarked = !widget.isBookmarked!;
              await widget.onBookmarkTap!();
              setState(() {});
            },
            iconData: widget.isBookmarked!
                ? Icons.bookmark
                : Icons.bookmark_border_outlined,
            iconSize: 30,
          ),
      ],
    );
  }
}

class PostCardFooter extends StatefulWidget {
  final int numberOfComments;
  final Function()? onLikeTap;
  final Function()? onCommentTap;

  bool? isLiked;
  int? numberOfLikes;

  PostCardFooter({
    Key? key,
    required this.numberOfComments,
    required this.onLikeTap,
    required this.onCommentTap,
    this.isLiked,
    this.numberOfLikes,
  }) : super(key: key);

  @override
  _PostCardFooterState createState() => _PostCardFooterState();
}

class _PostCardFooterState extends State<PostCardFooter> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        CustomIconButton(
          iconData:
              widget.isLiked! ? Icons.thumb_up : Icons.thumb_up_alt_outlined,
          iconSize: 18.0,
          onTap: () async {
            await widget.onLikeTap!();
            widget.isLiked = !widget.isLiked!;
            if (widget.isLiked!) {
              widget.numberOfLikes = widget.numberOfLikes! + 1;
            } else {
              widget.numberOfLikes = widget.numberOfLikes! - 1;
            }

            setState(() {});
          },
        ),
        SizedBox(width: Size.w(1)),
        Text(
          widget.numberOfLikes.toString(),
          style: kTheme.textTheme.bodyText2!
              .copyWith(color: kThemeController.neutralOnBackground),
        ),
        SizedBox(width: Size.w(3)),
        CustomIconButton(
          iconData: Icons.comment,
          iconSize: 18.0,
          onTap: widget.onCommentTap,
        ),
        SizedBox(width: Size.w(1)),
        Text(
          widget.numberOfComments.toString(),
          style: kTheme.textTheme.bodyText2!
              .copyWith(color: kThemeController.neutralOnBackground),
        )
      ],
    );
  }
}

class ShimmerPostCard extends StatelessWidget {
  const ShimmerPostCard({Key? key}) : super(key: key);

  _buildHeader() {
    return Row(
      children: [
        CustomShimmer.circular(width: Size.w(10), height: Size.w(10)),
        SizedBox(width: Size.w(2)),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomShimmer.rectangular(width: Size.w(40), height: Size.w(4)),
            SizedBox(height: Size.w(2)),
            CustomShimmer.rectangular(width: Size.w(20), height: Size.w(3)),
          ],
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ThemeController>(
      builder: (_) => Container(
        width: double.infinity,
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildHeader(),
            SizedBox(height: Size.h(1)),
            CustomShimmer.rectangular(height: Size.h(3)),
            SizedBox(height: Size.h(1)),
            CustomShimmer.rectangular(height: Size.h(10)),
          ],
        ),
      ),
    );
  }
}
