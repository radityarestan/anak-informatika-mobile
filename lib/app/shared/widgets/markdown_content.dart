import 'package:anak_informatika/app/shared/theme/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:url_launcher/url_launcher.dart';

class MarkdownContent extends StatelessWidget {
  final String data;
  final Color color;
  const MarkdownContent({
    Key? key,
    required this.data,
    required this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MarkdownBody(
      styleSheet: MarkdownStyleSheet.fromTheme(Theme.of(context)).copyWith(
        a: kTheme.textTheme.bodyText1!.copyWith(
          color: kThemeController.linkColor,
        ),
        p: kTheme.textTheme.bodyText1!.copyWith(
          color: color,
        ),
        h1: kTheme.textTheme.headline1!.copyWith(
          color: color,
        ),
        h2: kTheme.textTheme.headline2!.copyWith(
          color: color,
        ),
        h3: kTheme.textTheme.headline3!.copyWith(
          color: color,
        ),
        h4: kTheme.textTheme.headline4!.copyWith(
          color: color,
        ),
        h5: kTheme.textTheme.headline5!.copyWith(
          color: color,
        ),
        h6: kTheme.textTheme.subtitle1!.copyWith(
          color: color,
        ),
        listBullet: kTheme.textTheme.subtitle2!.copyWith(
          color: color,
        ),
        checkbox: kTheme.textTheme.subtitle2!.copyWith(
          color: color,
        ),
        tableHead: kTheme.textTheme.bodyText1!.copyWith(
          color: color,
        ),
        tableBody: kTheme.textTheme.bodyText1!.copyWith(
          color: color,
        ),
        blockquote: kTheme.textTheme.bodyText1!.copyWith(
          color: color,
        ),
        code: GoogleFonts.inconsolata().copyWith(
          fontSize: kTheme.textTheme.bodyText1!.fontSize,
          color: kThemeController.neutralOnBackground,
          backgroundColor: kThemeController.transparent,
        ),
        tableBorder: TableBorder.all(color: kThemeController.neutralBorder),
        blockquoteDecoration: BoxDecoration(
          color: kThemeController.neutralBorder,
        ),
        codeblockDecoration: BoxDecoration(
          color: kThemeController.neutralBorder,
        ),
        horizontalRuleDecoration: BoxDecoration(
          color: kThemeController.neutralSurface2,
        ),
      ),
      data: data,
      onTapLink: (text, href, title) => launch(href!),
    );
  }
}
