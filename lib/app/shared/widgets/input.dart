// ignore_for_file: must_be_immutable

import 'package:anak_informatika/app/shared/theme/theme.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class Input extends StatelessWidget {
  final String? Function(String?)? validator;
  final TextEditingController controller;
  final String inputField;
  final String? labelText;
  final String? hintText;
  final bool obscureText;
  final bool disabled;
  final double width;
  Icon? prefixIcon;
  Icon? suffixIcon;

  Input({
    Key? key,
    required this.width,
    required this.controller,
    required this.inputField,
    this.validator,
    this.labelText,
    this.hintText,
    this.prefixIcon,
    this.suffixIcon,
    this.disabled = false,
    this.obscureText = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (prefixIcon != null) {
      prefixIcon = Icon(
        prefixIcon!.icon,
        color: kThemeController.neutralOutline,
      );
    }

    if (suffixIcon != null) {
      suffixIcon = Icon(
        suffixIcon!.icon,
        color: kThemeController.neutralOutline,
      );
    }

    return SizedBox(
      width: width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (labelText != null)
            Padding(
              padding: const EdgeInsets.only(bottom: 5.0),
              child: Text(
                labelText!,
                style: kTheme.textTheme.bodyText1!
                    .copyWith(color: kThemeController.neutralOnBackground),
              ),
            ),
          TextFormField(
              enabled: !disabled,
              controller: controller,
              cursorColor: kThemeController.neutralOutline,
              validator: validator,
              autocorrect: false,
              obscureText: obscureText,
              style: kTheme.textTheme.bodyText1!
                  .copyWith(color: kThemeController.neutralOutline),
              decoration: InputDecoration(
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8.0)),
                enabledBorder: OutlineInputBorder(
                    borderSide:
                        BorderSide(color: kThemeController.neutralBorder)),
                focusedBorder: OutlineInputBorder(
                    borderSide:
                        BorderSide(color: kThemeController.neutralBorder)),
                errorStyle: kTheme.textTheme.caption!.copyWith(
                  color: kThemeController.errorMain,
                ),
                errorBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: kThemeController.errorMain)),
                errorMaxLines: 2,
                hintText: inputField,
                hintStyle: kTheme.textTheme.bodyText1!
                    .copyWith(color: kThemeController.neutralOutline),
                prefixIcon: prefixIcon,
                suffixIcon: suffixIcon,
                filled: true,
                fillColor: !disabled
                    ? kThemeController.neutralBackground
                    : kThemeController.neutralOnBackground.withOpacity(0.08),
              )),
          if (hintText != null)
            Padding(
              padding: const EdgeInsets.only(top: 5.0),
              child: AutoSizeText(
                hintText!,
                maxLines: 2,
                style: kTheme.textTheme.bodyText2!
                    .copyWith(color: kThemeController.neutralOutline),
              ),
            ),
        ],
      ),
    );
  }
}

class InputPost extends StatelessWidget {
  final String? Function(String?)? validator;
  final TextEditingController controller;
  final Function(String)? onChanged;
  final String inputField;
  final String? labelText;
  final String? hintText;
  final bool autofocus;
  final double width;

  const InputPost({
    Key? key,
    required this.width,
    required this.controller,
    required this.inputField,
    this.onChanged,
    this.validator,
    this.labelText,
    this.hintText,
    this.autofocus = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (labelText != null)
            Padding(
              padding: const EdgeInsets.only(bottom: 5.0),
              child: Text(
                labelText!,
                style: kTheme.textTheme.bodyText1!
                    .copyWith(color: kThemeController.neutralOnBackground),
              ),
            ),
          TextFormField(
              onChanged: onChanged,
              controller: controller,
              cursorColor: kThemeController.neutralOutline,
              validator: validator,
              autocorrect: false,
              autofocus: autofocus,
              maxLines: null,
              style: kTheme.textTheme.bodyText1!
                  .copyWith(color: kThemeController.neutralOnBackground),
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8.0)),
                  enabledBorder: OutlineInputBorder(
                      borderSide:
                          BorderSide(color: kThemeController.neutralBorder)),
                  focusedBorder: OutlineInputBorder(
                      borderSide:
                          BorderSide(color: kThemeController.neutralBorder)),
                  errorStyle: kTheme.textTheme.caption!.copyWith(
                    color: kThemeController.errorMain,
                  ),
                  errorBorder: OutlineInputBorder(
                      borderSide:
                          BorderSide(color: kThemeController.errorMain)),
                  errorMaxLines: 2,
                  hintText: inputField,
                  hintStyle: kTheme.textTheme.bodyText1!
                      .copyWith(color: kThemeController.neutralOutline),
                  filled: true,
                  fillColor: kThemeController.neutralBackground)),
          if (hintText != null)
            Padding(
              padding: const EdgeInsets.only(top: 5.0),
              child: AutoSizeText(
                hintText!,
                maxLines: 2,
                style: kTheme.textTheme.bodyText2!
                    .copyWith(color: kThemeController.neutralOutline),
              ),
            ),
        ],
      ),
    );
  }
}
