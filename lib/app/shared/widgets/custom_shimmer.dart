import 'package:anak_informatika/app/shared/theme/constant.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class CustomShimmer extends StatelessWidget {
  final double width;
  final double height;
  final ShapeBorder shapeBorder;

  const CustomShimmer.rectangular({
    Key? key,
    required this.height,
    this.width = double.infinity,
    this.shapeBorder = const RoundedRectangleBorder(),
  }) : super(key: key);

  const CustomShimmer.circular({
    Key? key,
    required this.width,
    required this.height,
    this.shapeBorder = const CircleBorder(),
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: kThemeController.neutralSurface2,
      highlightColor: kThemeController.white,
      child: Container(
        width: width,
        height: height,
        decoration: ShapeDecoration(
          color: kThemeController.neutralOutline,
          shape: shapeBorder,
        ),
      ),
    );
  }
}
