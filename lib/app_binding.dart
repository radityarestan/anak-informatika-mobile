import 'package:anak_informatika/app/modules/settings/settings.dart';
import 'package:anak_informatika/app/services/api_provider.dart';
import 'package:anak_informatika/app/shared/theme/theme_controller.dart';
import 'package:get/get.dart';

class AppBinding extends Bindings {
  @override
  void dependencies() async {
    Get.put(ThemeController());
    Get.put(TranslationController());
    Get.put(ApiProvider());
  }
}
