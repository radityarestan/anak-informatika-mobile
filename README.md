# Anak Informatika

A social media project.

## Keterangan

This project is using Flutter as a mobile framework.

A few resources to get you started with this project:
- [GetX State Management](https://pub.dev/packages/get)
- [MVC Pattern](https://www.tutorialspoint.com/design_pattern/mvc_pattern.htm)
- [Firebase](https://medium.com/firebase-developers/what-is-firebase-the-complete-story-abridged-bcc730c5f2c0)